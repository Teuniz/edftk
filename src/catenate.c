/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <limits.h>

#include "utils.h"
#include "catenate.h"
#include "cmp_hdrs.h"
#include "edflib.h"
#include "sort_files.h"
#include "annotations.h"

#include <assert.h>


extern volatile sig_atomic_t sig_flag;


typedef struct
{
  long long smpl_start[EDFLIB_MAXSIGNALS];
  long long num_smpls_dat[EDFLIB_MAXSIGNALS];
  long long smpl_end[EDFLIB_MAXSIGNALS];
  long long datrec_idx_start[EDFLIB_MAXSIGNALS];
  int smp_in_datarecord[EDFLIB_MAXSIGNALS];
  int datrec_pre_dat;
  long long num_datrecs_dat;
  long long num_datrecs_zero;
  long long datrec_seq_start;
  long long datrec_seq_end;
} out_layout_t;


static int check_signalrange_arg(const char *);
static int parse_signalrange(prg_args_t *, f_prop_t *, int, int *, int *);
static int init_edf_hdr(prg_args_t *, f_prop_t *);
static int init_smpl_ranges(f_prop_t *, out_layout_t *);
static int copy_smpl_data(f_prop_t *, out_layout_t *);
static void tidy_up(f_prop_t *, out_layout_t *);
static int get_datarec(f_prop_t *, out_layout_t *, int);
static int write_baseline_datrecs(f_prop_t *, int);


int catenate_files(prg_args_t *a, f_prop_t *p)
{
  int i, j, k, n,
      sig_idx_min=0,
      sig_idx_max=0,
      signal_range_set=0;

  out_layout_t *ol=NULL;

  if(a->out_fname == NULL)
  {
    fprintf(stderr, "error: missing argument output file\n");
    return -1;
  }

  sort_input_files(p);

  for(i=0; i<a->op_arg_cnt; i++)
  {
    if(strlen(a->op_arg[i]) > 11 && !strncmp(a->op_arg[i], "maxtimegap=", 11))
    {
      p->timegap_err_trh = hhmmsstosec(a->op_arg[i] + 11);

      if(p->timegap_err_trh < 0)
      {
        fprintf(stderr, "error: unrecognized or invalid operation argument: %s\n", a->op_arg[i]);
        return -2;
      }
    }
    else if(strlen(a->op_arg[i]) > 12 && !strncmp(a->op_arg[i], "maxduration=", 12))
      {
        fprintf(stderr, "error: operation argument 'maxduration' cannot be used with operation 'cat'.\n"
                        "use operation 'merge' instead.\n");
        return -3;
      }
      else if(check_signalrange_arg(a->op_arg[i]))
        {
          fprintf(stderr, "error: unrecognized or invalid operation argument: %s\n", a->op_arg[i]);
          return -5;
        }
  }

  if(cmp_hdrs(p))
  {
    fprintf(stderr, "error: cannot catenate input files because they are not identical\n");
    return -6;
  }

  for(i=0; i<(p->in_f_cnt-1); i++)
  {
    if(p->endtime_utc[i] > p->starttime_utc[i+1])
    {
      fprintf(stderr, "error: input files overlap in time,\n"
                      "end time of file %s is after start time of file %s\n",
      p->in_fname[i], p->in_fname[i+1]);
      return -7;
    }

    if(((p->starttime_utc[i+1] - p->endtime_utc[i]) / EDFLIB_TIME_DIMENSION) > p->timegap_err_trh)
    {
      fprintf(stderr, "error: time between end of file %s and start of file %s is more than %lli seconds\n",
      p->in_fname[i], p->in_fname[i+1], p->timegap_err_trh);
      return -8;
    }
  }

  if(a->op_arg_cnt)
  {
    for(n=0; n<a->op_arg_cnt; n++)
    {
      if((!strncmp(a->op_arg[n], "maxtimegap=", 11)) || (!strncmp(a->op_arg[n], "maxduration=", 12)))  continue;

      if(parse_signalrange(a, p, n, &sig_idx_min, &sig_idx_max))
      {
        fprintf(stderr, "error: parsing signal range: %s\n", a->op_arg[i]);
        return -9;
      }

      if((sig_idx_min < 1) || (sig_idx_max > p->hdr_in[0].edfsignals))
      {
        fprintf(stderr, "error: operation argument is out of range: %s\n", a->op_arg[i]);
        return -10;
      }

      sig_idx_min--;
      sig_idx_max--;

      for(j=0; j<p->in_f_cnt; j++)
      {
        for(i=0; i<p->hdr_in[j].edfsignals; i++)
        {
          if((i >= sig_idx_min) && (i <= sig_idx_max))
          {
            p->sig_sel[j][i] = 1;
          }
        }
      }

      signal_range_set = 1;
    }
  }

  if(!signal_range_set)
  {
    sig_idx_min = 0;

    sig_idx_max = p->hdr_in[0].edfsignals - 1;

    for(j=0; j<p->in_f_cnt; j++)
    {
      for(i=0; i<p->hdr_in[j].edfsignals; i++)
      {
        p->sig_sel[j][i] = 1;
        p->sig_idx[j][i] = i;
      }
    }
  }

  for(j=0; j<p->in_f_cnt; j++)
  {
    for(i=0, k=0; i<p->hdr_in[j].edfsignals; i++)
    {
      if(p->sig_sel[j][i])
      {
        p->sig_idx[j][k++] = i;
      }
    }
  }

  for(i=0, p->num_sig_out=0, p->buf_total_smpl_sz=0; i<p->hdr_in[0].edfsignals; i++)
  {
    if(p->sig_sel[0][i])
    {
      p->buf_offset[p->num_sig_out] = p->buf_total_smpl_sz;
      p->buf_total_smpl_sz += p->hdr_in[0].signalparam[i].smp_in_datarecord;
      p->num_sig_out++;
    }
  }

  p->out_fname = a->out_fname;

  ol = (out_layout_t *)calloc(1, sizeof(out_layout_t[MAX_INPUT_FILES]));
  if(ol == NULL)
  {
    fprintf(stderr, "malloc error: init_smpl_ranges()\n");
    goto CAT_OUT_ERROR;
  }

  if(init_smpl_ranges(p, ol))
  {
    goto CAT_OUT_ERROR;
  }

  if(init_edf_hdr(a, p))
  {
    edfclose_file(p->hdl_out);
    p->hdl_out = -1;
    fprintf(stderr, "error: cannot initialize header of output file\n");
    goto CAT_OUT_ERROR;
  }

  if(set_annot_chns(p))
  {
    edfclose_file(p->hdl_out);
    p->hdl_out = -1;
    fprintf(stderr, "error: cannot initialize annotation channels of output file\n");
    goto CAT_OUT_ERROR;
  }

  p->buf_in = (int *)malloc(sizeof(int) * p->buf_total_smpl_sz);
  if(p->buf_in == NULL)
  {
    fprintf(stderr, "error: malloc()\n");
    goto CAT_OUT_ERROR;
  }

  p->buf_out = (int *)malloc(sizeof(int) * p->buf_total_smpl_sz);
  if(p->buf_out == NULL)
  {
    fprintf(stderr, "error: malloc()\n");
    goto CAT_OUT_ERROR;
  }

  if(copy_smpl_data(p, ol))
  {
    if(!sig_flag)  fprintf(stderr, "error: copy_smpl_data\n");
    goto CAT_OUT_ERROR;
  }

  copy_annotations(p);

  tidy_up(p, ol);
  return 0;

CAT_OUT_ERROR:

  tidy_up(p, ol);
  return -99;
}


static void tidy_up(f_prop_t *p, out_layout_t *ol)
{
  if(p->hdl_out >= 0)
  {
    edfclose_file(p->hdl_out);
    p->hdl_out = -1;
  }

  free(p->buf_in);
  p->buf_in = NULL;
  free(p->buf_out);
  p->buf_out = NULL;
  free(ol);
  ol = NULL;
}

/* Only returns 0 when the string argument contains
 * either: "3", "5-18" or "4-end". '0' is not allowed.
 */
static int check_signalrange_arg(const char *s)
{
  int digit_b=0, digit_a=0, dash=0, end=0;

  if(!atoi(s))  return -1;

  for(; *s != 0; s++)
  {
    if(isdigit(*s))
    {
      if(end)  return -2;

      if(dash)
      {
        if(!digit_a)
        {
          if(!atoi(s))  return -3;
        }
        digit_a++;
      }
      else
      {
        digit_b++;
      }
    }
    else if(*s == '-')
      {
        if(end || !digit_b || dash) return -4;
        dash++;
      }
      else if(!strcmp(s, "end"))
        {
          if(end || !dash || !digit_b || digit_a)  return -5;
          end++;
          s += 2;
        }
        else
        {
          return -6;
        }
  }

  if(!digit_b || (dash && !digit_a && !end)) return -7;

  return 0;
}


static int parse_signalrange(prg_args_t *a, f_prop_t *p, int idx, int *min, int *max)
{
  int tmp;

  const char *c_p=NULL;

  *min = atoi(a->op_arg[idx]);

  c_p = strstr(a->op_arg[idx], "-");
  if(c_p == NULL)
  {
    *max = *min;
  }
  else
  {
    c_p++;
    if(!strcmp(c_p, "end"))
    {
      *max = p->hdr_in[0].edfsignals;
    }
    else
    {
      *max = atoi(c_p);
    }
  }

  if(*min > *max)
  {
    tmp = *min;
    *min = *max;
    *max = tmp;
  }

  return 0;
}


static int init_edf_hdr(prg_args_t *a, f_prop_t *p)
{
  int i, sig_out=0;

  if((p->hdr_in[0].filetype == EDFLIB_FILETYPE_EDF) || (p->hdr_in[0].filetype == EDFLIB_FILETYPE_EDFPLUS))
  {
    p->hdl_out = edfopen_file_writeonly(a->out_fname, EDFLIB_FILETYPE_EDFPLUS, p->num_sig_out);
  }
  else
  {
    p->hdl_out = edfopen_file_writeonly(a->out_fname, EDFLIB_FILETYPE_BDFPLUS, p->num_sig_out);
  }
  if(p->hdl_out < 0)
  {
    fprintf(stderr, "error while opening output file: EDFlib: ");

    switch(p->hdl_out)
    {
      case EDFLIB_MALLOC_ERROR              : fprintf(stderr, "malloc error\n");
                                              break;
      case EDFLIB_NO_SUCH_FILE_OR_DIRECTORY : fprintf(stderr, "no such file or directory\n");
                                              break;
      case EDFLIB_MAXFILES_REACHED          : fprintf(stderr, "maximum number of files reached\n");
                                              break;
      case EDFLIB_FILE_ALREADY_OPENED       : fprintf(stderr, "file already opened\n");
                                              break;
      case EDFLIB_NUMBER_OF_SIGNALS_INVALID : fprintf(stderr, "number of signals invalid\n");
                                              break;
      case EDFLIB_ARCH_ERROR                : fprintf(stderr, "architectural error\n");
                                              break;
      default                               : fprintf(stderr, "unknown error\n");
                                              break;
    }

    return -1;
  }

  if(p->hdr_in[0].datarecord_duration >= 100LL)
  {
    if(edf_set_datarecord_duration(p->hdl_out, p->hdr_in[0].datarecord_duration / 100))
    {
      fprintf(stderr, "error: edf_set_datarecord_duration()\n");
      return -2;
    }
  }
  else if(p->hdr_in[0].datarecord_duration >= 10LL)
    {
      if(edf_set_micro_datarecord_duration(p->hdl_out, p->hdr_in[0].datarecord_duration / 10))
      {
        fprintf(stderr, "error: edf_set_micro_datarecord_duration()\n");
        return -3;
      }
    }
    else
    {
      fprintf(stderr, "error: datarecord duration of input file is too short\n");
      return -4;
    }

  if(edf_set_startdatetime(p->hdl_out, p->hdr_in[0].startdate_year, p->hdr_in[0].startdate_month, p->hdr_in[0].startdate_day,
                                   p->hdr_in[0].starttime_hour, p->hdr_in[0].starttime_minute, p->hdr_in[0].starttime_second))
  {
    fprintf(stderr, "error: edf_set_startdatetime()\n");
    return -5;
  }

  if(edf_set_annot_chan_idx_pos(p->hdl_out, EDF_ANNOT_IDX_POS_START))
  {
    fprintf(stderr, "error: edf_set_annot_chan_idx_pos()\n");
    return -6;
  }

  if(edf_set_subsecond_starttime(p->hdl_out, p->hdr_in[0].starttime_subsecond))
  {
    fprintf(stderr, "error: edf_set_subsecond_starttime()\n");
    return -7;
  }

  if((p->hdr_in[0].filetype == EDFLIB_FILETYPE_EDF) || (p->hdr_in[0].filetype == EDFLIB_FILETYPE_BDF))
  {
    if(edf_set_patient_additional(p->hdl_out, p->hdr_in[0].patient))
    {
      fprintf(stderr, "error: edf_set_patient_additional()\n");
      return -8;
    }

    if(edf_set_recording_additional(p->hdl_out, p->hdr_in[0].recording))
    {
      fprintf(stderr, "error: edf_set_recording_additional()\n");
      return -9;
    }
  }
  else
  {
    if(edf_set_patientname(p->hdl_out, p->hdr_in[0].patient_name))
    {
      fprintf(stderr, "error: edf_set_patientname()\n");
      return -10;
    }

    if(!strcmp(p->hdr_in[0].sex, "Male"))
    {
      if(edf_set_sex(p->hdl_out, 1))
      {
        fprintf(stderr, "error: edf_set_sex()\n");
        return -11;
      }
    }
    else if(!strcmp(p->hdr_in[0].sex, "Female"))
      {
        if(edf_set_sex(p->hdl_out, 0))
        {
          fprintf(stderr, "error: edf_set_sex()\n");
          return -12;
        }
      }

    if(p->hdr_in[0].birthdate_day)
    {
      if(edf_set_birthdate(p->hdl_out, p->hdr_in[0].birthdate_year, p->hdr_in[0].birthdate_month, p->hdr_in[0].birthdate_day))
      {
        fprintf(stderr, "error: edf_set_birthdate()\n");
        return -13;
      }
    }

    if(edf_set_patient_additional(p->hdl_out, p->hdr_in[0].patient_additional))
    {
      fprintf(stderr, "error: edf_set_patient_additional()\n");
      return -14;
    }

    if(edf_set_admincode(p->hdl_out, p->hdr_in[0].admincode))
    {
      fprintf(stderr, "error: edf_set_admincode()\n");
      return -15;
    }

    if(edf_set_technician(p->hdl_out, p->hdr_in[0].technician))
    {
      fprintf(stderr, "error: edf_set_technician()\n");
      return -16;
    }

    if(edf_set_equipment(p->hdl_out, p->hdr_in[0].equipment))
    {
      fprintf(stderr, "error: edf_set_equipment()\n");
      return -17;
    }

    if(edf_set_recording_additional(p->hdl_out, p->hdr_in[0].recording_additional))
    {
      fprintf(stderr, "error: edf_set_recording_additional()\n");
      return -18;
    }
  }

  for(i=0, sig_out=0; i<p->hdr_in[0].edfsignals; i++)
  {
    if(p->sig_sel[0][i])
    {
      if(edf_set_samplefrequency(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].smp_in_datarecord))
      {
        fprintf(stderr, "error: edf_set_samplefrequency()\n");
        return -51;
      }

      if(edf_set_digital_maximum(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].dig_max))
      {
        fprintf(stderr, "error: edf_set_digital_maximum()\n");
        return -52;
      }

      if(edf_set_digital_minimum(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].dig_min))
      {
        fprintf(stderr, "error: edf_set_digital_minimum()\n");
        return -53;
      }

      if(edf_set_physical_maximum(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].phys_max))
      {
        fprintf(stderr, "error: edf_set_physical_maximum()\n");
        return -54;
      }

      if(edf_set_physical_minimum(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].phys_min))
      {
        fprintf(stderr, "error: edf_set_physical_minimum()\n");
        return -55;
      }

      if(edf_set_label(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].label))
      {
        fprintf(stderr, "error: edf_set_label()\n");
        return -56;
      }

      if(edf_set_physical_dimension(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].physdimension))
      {
        fprintf(stderr, "error: edf_set_physical_dimension()\n");
        return -57;
      }

      if(edf_set_transducer(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].transducer))
      {
        fprintf(stderr, "error: edf_set_transducer()\n");
        return -58;
      }

      if(edf_set_prefilter(p->hdl_out, sig_out, p->hdr_in[0].signalparam[i].prefilter))
      {
        fprintf(stderr, "error: edf_set_prefilter()\n");
        return -59;
      }

      sig_out++;
    }
  }

  return 0;
}


static int init_smpl_ranges(f_prop_t *p, out_layout_t *ol)
{
  int s_n;

  long long t_start,
            utc_start=INT64_MAX,
            utc_end=0;

  double part;

  for(int f=0; f<p->in_f_cnt; f++)
  {
    if(utc_start > p->starttime_utc[f])  utc_start = p->starttime_utc[f];

    if(utc_end < p->endtime_utc[f])  utc_end = p->endtime_utc[f];
  }

  p->datrec_duration = p->hdr_in[0].datarecord_duration;

  if(p->rec_max_len)
  {
    p->rec_datrecs_dur_lim = p->rec_datrecs_start + ((p->rec_max_len * EDFLIB_TIME_DIMENSION) / p->datrec_duration);
  }
  else
  {
    p->rec_datrecs_dur_lim = LLONG_MAX;
  }

  ol[0].num_datrecs_dat = p->hdr_in[0].datarecords_in_file;

  ol[0].datrec_seq_start = 0;

  ol[0].datrec_seq_end = ol[0].num_datrecs_dat;

  p->datrecs_out = (utc_end - utc_start) / p->datrec_duration;

  for(int i=0; i<p->num_sig_out; i++)
  {
    s_n = p->sig_idx[0][i];

    ol[0].smp_in_datarecord[i] = p->hdr_in[0].signalparam[s_n].smp_in_datarecord;

    ol[0].smpl_start[i] = 0;

    ol[0].num_smpls_dat[i] = p->hdr_in[0].signalparam[s_n].smp_in_file;

    ol[0].smpl_end[i] = ol[0].num_smpls_dat[i];

    ol[0].datrec_idx_start[i] = 0;

    ol[0].num_datrecs_zero = 0;

    ol[0].datrec_pre_dat = 0;
  }

  for(int f=1; f<p->in_f_cnt; f++)
  {
    t_start = p->starttime_utc[f] - p->starttime_utc[0];

    ol[f].datrec_seq_start = t_start / p->datrec_duration;

    part = (t_start % p->datrec_duration) / (double)p->datrec_duration;

    ol[f].num_datrecs_dat = p->hdr_in[f].datarecords_in_file;

    ol[f].datrec_seq_end = ol[f].datrec_seq_start + ol[f].num_datrecs_dat;

    ol[f].datrec_pre_dat = 0;

    ol[f-1].num_datrecs_zero = ol[f].datrec_seq_start - ol[f-1].datrec_seq_end;

    for(int i=0; i<p->num_sig_out; i++)
    {
      s_n = p->sig_idx[f][i];

      ol[f].smp_in_datarecord[i] = p->hdr_in[f].signalparam[s_n].smp_in_datarecord;

      ol[f].smpl_start[i] = ol[f].datrec_seq_start * ol[f].smp_in_datarecord[i];

      ol[f].smpl_start[i] += part * ol[f].smp_in_datarecord[i];

      ol[f].num_smpls_dat[i] = p->hdr_in[f].signalparam[s_n].smp_in_file;

      ol[f].smpl_end[i] = ol[f].smpl_start[i] + ol[f].num_smpls_dat[i];

      ol[f].datrec_idx_start[i] = ol[f].smpl_start[i] % ol[f].smp_in_datarecord[i];

      if(ol[f].datrec_idx_start[i])  ol[f].datrec_pre_dat = 1;
    }
  }

  ol[p->in_f_cnt-1].num_datrecs_zero = 0;

  return 0;
}


static int get_datarec(f_prop_t *p, out_layout_t *ol, int f)
{
  for(int sig_in=0, sig_out=0; sig_out<p->num_sig_out; sig_out++)
  {
    sig_in = p->sig_idx[f][sig_out];

    if(edfread_digital_samples(p->hdl_in[f], sig_in,
       ol[f].smp_in_datarecord[sig_out], &p->buf_in[p->buf_offset[sig_out]])
       != ol[f].smp_in_datarecord[sig_out])
    {
      fprintf(stderr, "error: edfread_digital_samples(): cannot read samples of signal %i from file %s\n",
              sig_in + 1, p->in_fname[f]);
      return -1;
    }
  }

  return 0;
}


int copy_smpl_data(f_prop_t *p, out_layout_t *ol)
{
  long long datrec_seq=0;

  for(int f=0; f<p->in_f_cnt; f++)
  {
    // for(int i=0; i<p->num_sig_out; i++)  //FIXME
    // {
    //   fprintf(stderr, "sig %i smp_in_datarecord: %i\n", i + 1, ol[f].smp_in_datarecord[i]);
    //   fprintf(stderr, "sig %i buf_offset: %i\n", i + 1, p->buf_offset[i]);
    //   fprintf(stderr, "sig %i datrec_idx_start: %lli\n\n", i + 1, ol[f].datrec_idx_start[i]);
    // }
    //
    // fprintf(stderr, "\nfile: %s\n", p->in_fname[f]);  //FIXME
    // fprintf(stderr, "datrec_seq_start: %lli\n", ol[f].datrec_seq_start);
    // fprintf(stderr, "datrec_seq_end: %lli\n", ol[f].datrec_seq_end);
    // fprintf(stderr, "datrec_pre_dat: %i\n", ol[f].datrec_pre_dat);
    // fprintf(stderr, "num_datrecs_dat: %lli\n", ol[f].num_datrecs_dat);
    // fprintf(stderr, "num_datrecs_zero: %lli\n\n", ol[f].num_datrecs_zero);

    if(sig_flag)  break;

    assert(datrec_seq == ol[f].datrec_seq_start);

    for(long long datrec=0; datrec<ol[f].num_datrecs_dat; datrec++)
    {
      if(sig_flag)  break;

      if(get_datarec(p, ol, f))
      {
        fprintf(stderr, "error: get_datarec(): cannot read samples from datarecord %lli in file %s\n",
                datrec + 1, p->in_fname[0]);
        return -1;
      }

      if(!ol[f].datrec_pre_dat)
      {
        if(edf_blockwrite_digital_samples(p->hdl_out, p->buf_in))
        {
          fprintf(stderr, "error: edf_blockwrite_digital_samples() cannot write samples of datarecord %lli to file %s\n",
                  datrec + 1, p->out_fname);
          return -2;
        }
        datrec_seq++;
      }
      else  /* datrec_pre_dat */
      {
        if(!datrec)
        {
          if(f)
          {
            if((!ol[f-1].datrec_pre_dat) && (ol[f-1].num_datrecs_zero < 2))
            {
              memset(p->buf_out, 0, sizeof(int) * p->buf_total_smpl_sz);
            }
          }
        }

        for(int sig_out=0; sig_out<p->num_sig_out; sig_out++)
        {
          for(int smpl=ol[f].datrec_idx_start[sig_out], j=0; smpl<ol[f].smp_in_datarecord[sig_out]; smpl++, j++)
          {
            p->buf_out[p->buf_offset[sig_out] + smpl] = p->buf_in[p->buf_offset[sig_out] + j];
          }
        }

        if(edf_blockwrite_digital_samples(p->hdl_out, p->buf_out))
        {
          fprintf(stderr, "error: edf_blockwrite_digital_samples() cannot write samples of datarecord %lli to file %s\n",
                  datrec + 1, p->out_fname);
          return -3;
        }
        datrec_seq++;

        for(int sig_out=0; sig_out<p->num_sig_out; sig_out++)
        {
          for(int smpl=0, j=ol[f].smp_in_datarecord[sig_out]-ol[f].datrec_idx_start[sig_out]; smpl<ol[f].datrec_idx_start[sig_out]; smpl++, j++)
          {
            p->buf_out[p->buf_offset[sig_out] + smpl] = p->buf_in[p->buf_offset[sig_out] + j];
          }
        }
      }
    }

    assert(datrec_seq == ol[f].datrec_seq_end);

    if(ol[f].datrec_pre_dat)
    {
      for(int sig_out=0; sig_out<p->num_sig_out; sig_out++)
      {
        for(int smpl=ol[f].datrec_idx_start[sig_out]; smpl<ol[f].smp_in_datarecord[sig_out]; smpl++)
        {
          p->buf_out[p->buf_offset[sig_out] + smpl] = 0;
        }
      }

      if(ol[f].num_datrecs_zero || f == p->in_f_cnt - 1)
      {
        if(edf_blockwrite_digital_samples(p->hdl_out, p->buf_out))
        {
          fprintf(stderr, "error: edf_blockwrite_digital_samples() cannot write samples to file %s\n",
                  p->out_fname);
          return -4;
        }
        datrec_seq++;
      }

      if(ol[f].num_datrecs_zero > 1)
      {
        if(write_baseline_datrecs(p, ol[f].num_datrecs_zero - 1))
        {
          fprintf(stderr, "error: write_baseline_datrecs() cannot write baseline samples of datarecord to file %s\n",
                  p->out_fname);
          return -5;
        }
        datrec_seq += ol[f].num_datrecs_zero - 1;
      }
    }
    else  /* !datrec_pre_dat */
    {
      if(ol[f].num_datrecs_zero)
      {
        if(write_baseline_datrecs(p, ol[f].num_datrecs_zero))
        {
          fprintf(stderr, "error: write_baseline_datrecs() cannot write baseline samples of datarecord to file %s\n",
                  p->out_fname);
          return -6;
        }
        datrec_seq += ol[f].num_datrecs_zero;
      }
    }
  }

  if(sig_flag)
  {
    fprintf(stderr, "\naborted\n");
    return -77;
  }

  return 0;
}


static int write_baseline_datrecs(f_prop_t *p, int n)
{
  memset(p->buf_out, 0, sizeof(int) * p->buf_total_smpl_sz);

  for(long long datrec=0; datrec<n; datrec++)
  {
    if(sig_flag)  break;

    if(edf_blockwrite_digital_samples(p->hdl_out, p->buf_out))
    {
      fprintf(stderr, "error: edf_blockwrite_digital_samples() cannot write baseline samples of datarecord %lli to file %s\n",
              datrec + 1, p->out_fname);
      return -1;
    }
  }

  return 0;
}








