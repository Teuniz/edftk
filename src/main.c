/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <unistd.h>
#include <signal.h>

#include "global.h"
#include "edflib.h"
#include "utils.h"
#include "utc_date_time.h"
#include "open_files.h"
#include "list_hdrs.h"
#include "help.h"
#include "parse_args.h"
#include "merge.h"
#include "catenate.h"
#include "dump.h"

/*
 * Please do NOT remove/change the following macro's.
 * The scope of this software is GNU/Linux only.
 * Please do NOT port it to other platforms/tools.
 * Also, do NOT send me patches that make it possible
 * to run this software on other operating systems.
 * I mean it.
 */
#ifndef __linux__
#error "OS must be Linux"
#endif

#if __BYTE_ORDER__ != __ORDER_LITTLE_ENDIAN__
#error "system byteorder must be little endian"
#endif

#if __GNUC__ < 7
#error "compiler must be GCC version >= 7"
#endif


volatile sig_atomic_t sig_flag=0;

void signal_catch_func(int);

prg_args_t prg_args;

f_prop_t f_props;



int main(int argc, const char **argv)
{
  int i;

  setlocale(LC_ALL, "C");

  setlinebuf(stdout);
  setlinebuf(stderr);

  if(argc == 1)
  {
    print_help(HELP_SYNOPSIS);
    exit(EXIT_FAILURE);
  }

  if(argc == 2)
  {
    if(!strcmp(argv[1], "--help"))
    {
      print_help(HELP_COMPLETE);
      exit(EXIT_SUCCESS);
    }
  }

  signal(SIGINT, signal_catch_func);

  memset(&prg_args, 0, sizeof(prg_args_t));

  memset(&f_props, 0, sizeof(f_prop_t));

  f_props.timegap_err_trh = TIMEGAP_ERR_TRHESHOLD;

  if(parse_prog_args(argc, argv, &prg_args))  return EXIT_FAILURE;

  if(strcmp(prg_args.op, "check") &&
     strcmp(prg_args.op, "list")  &&
     strcmp(prg_args.op, "cat")   &&
     strcmp(prg_args.op, "merge") &&
     strcmp(prg_args.op, "dump"))
  {
    fprintf(stderr, "Invalid operation: %s\n", prg_args.op);
    return EXIT_FAILURE;
  }

  if(!strcmp(prg_args.op, "dump"))
  {
    if(dump_datrec(&prg_args))
    {
      goto OUT_ERROR;
    }

    goto OUT_OK;
  }

  if(open_input_files(&prg_args, &f_props))  return EXIT_FAILURE;

  if(!strcmp(prg_args.op, "check"))
  {
    if(prg_args.op_arg_cnt)
    {
      fprintf(stderr, "invalid operation argument\n");
      goto OUT_ERROR;
    }

    goto OUT_OK;
  }

  if(!strcmp(prg_args.op, "list"))
  {
    if(list_headers(&prg_args, &f_props))
    {
      goto OUT_ERROR;
    }

    goto OUT_OK;
  }

  if(!strcmp(prg_args.op, "merge"))
  {
    if(merge_files(&prg_args, &f_props))
    {
      goto OUT_ERROR;
    }

    goto OUT_OK;
  }

  if(!strcmp(prg_args.op, "cat"))
  {
    if(catenate_files(&prg_args, &f_props))
    {
      goto OUT_ERROR;
    }

    goto OUT_OK;
  }

  fprintf(stderr, "Invalid operation: %s\n", prg_args.op);
  goto OUT_ERROR;

OUT_OK:

  for(i=0; i<f_props.in_f_cnt; i++)
  {
    if(f_props.hdl_in[i] >= 0)
    {
      edfclose_file(f_props.hdl_in[i]);
    }
  }

  return EXIT_SUCCESS;

OUT_ERROR:

  for(i=0; i<f_props.in_f_cnt; i++)
  {
    if(f_props.hdl_in[i] >= 0)
    {
      edfclose_file(f_props.hdl_in[i]);
    }
  }

  return EXIT_FAILURE;
}


void signal_catch_func(__attribute__((unused)) int sig)
{
  sig_flag = 1;
}






















