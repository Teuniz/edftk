/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <limits.h>
#include <errno.h>
#include <ctype.h>

#include "utils.h"
#include "edflib.h"
#include "dump.h"


#define HEXMODE     (0)
#define DECMODE     (1)
#define PHYSMODE    (2)
#define RAWMODE     (3)


typedef struct
{
  int total_chans;
  int regular_chans;
  int annot_chans;
  int regular_chans_idx_list[EDFLIB_MAXSIGNALS];
  int annot_chans_idx_list[EDFLIB_MAXSIGNALS];
  int bytes_in_datrec[EDFLIB_MAXSIGNALS];
  int smpls_in_datrec[EDFLIB_MAXSIGNALS];
  int datrec_offset[EDFLIB_MAXSIGNALS];
  int is_annot_ch[EDFLIB_MAXSIGNALS];
  int digmax[EDFLIB_MAXSIGNALS];
  int digmin[EDFLIB_MAXSIGNALS];
  double physmax[EDFLIB_MAXSIGNALS];
  double physmin[EDFLIB_MAXSIGNALS];
  double phys_resolution[EDFLIB_MAXSIGNALS];
  double phys_offset[EDFLIB_MAXSIGNALS];
  int hdr_sz;
  int datrecs;
  int datrec_sz;
  int sample_width;
  long long datrec_duration;
} edfhdr_t;

static int get_raw_header(const char *, edfhdr_t *);
static long long get_file_offset(int, int, int, edfhdr_t *);
static int get_datrec_data(const char *, void **, int, edfhdr_t *);
static void write_hex(void *, int, int);
static void write_dec(void *, int, int);
static void write_phys(void *, int, int, edfhdr_t *, int);


extern volatile sig_atomic_t sig_flag;


int dump_datrec(prg_args_t *a)
{
  int err,
      mode=0,
      datrec=0,
      chan=0,
      smpl=0,
      use_chan=0,
      use_smpl=0,
      use_time_based_idx=0,
      time_based_smpls=0,
      time_based_datrec_offset=0;

  char *buf1=NULL,
       *buf2=NULL;

  long long dump_time_offset,
            time_based_datrecs;

  edfhdr_t *hdr=NULL;

  if(a->in_f_cnt > 1)
  {
    fprintf(stderr, "error: too many input files\n");
    return -1;
  }

  if(a->op_arg_cnt < 2)
  {
    fprintf(stderr, "error: missing operation argument\n");
    return -2;
  }

  if(!strcmp(a->op_arg[0], "hex"))
  {
    mode = HEXMODE;
  }
  else if(!strcmp(a->op_arg[0], "dec"))
    {
      mode = DECMODE;
    }
    else if(!strcmp(a->op_arg[0], "phys"))
      {
        mode = PHYSMODE;
      }
      else if(!strcmp(a->op_arg[0], "raw"))
        {
          mode = RAWMODE;
        }
        else
        {
          fprintf(stderr, "error: unknow argument '%s'\n", a->op_arg[0]);
          return -3;
        }

  dump_time_offset = hhmmssuutousec(a->op_arg[1]);
  if(dump_time_offset >= 0)
  {
    use_time_based_idx = 1;

    dump_time_offset *= 10LL;
  }

  if(use_time_based_idx)
  {
    if(a->op_arg_cnt != 4)
    {
      fprintf(stderr, "error: time based index needs 2 arguments\n");
      return -5;
    }

    for(int i=2; i<a->op_arg_cnt; i++)
    {
      if(i > 3)
      {
        fprintf(stderr, "error: too many operation arguments\n");
        return -6;
      }

      if(!isintnum(a->op_arg[i]))
      {
        fprintf(stderr, "error: operation arguments must be integer numbers\n");
        return -7;
      }
    }
  }
  else  /* datarecord based index */
  {
    for(int i=1; i<a->op_arg_cnt; i++)
    {
      if(i > 3)
      {
        fprintf(stderr, "error: too many operation arguments\n");
        return -4;
      }

      if(!isintnum(a->op_arg[i]))
      {
        fprintf(stderr, "error: datarecord based index operation arguments must be integer numbers\n");
        return -5;
      }
    }
  }

  hdr = (edfhdr_t *)calloc(1, sizeof(edfhdr_t));
  if(hdr == NULL)
  {
    fprintf(stderr, "malloc error\n");
    return -6;
  }

  err = get_raw_header(a->in_fname[0], hdr);
  if(err)
  {
    fprintf(stderr, "error: cannot read header or header is corrupt, error: %i\n", err);
    err = -7;
    goto DUMP_OUT_EXIT;
  }

  if(use_time_based_idx)
  {
    if(dump_time_offset >= hdr->datrecs * hdr->datrec_duration)
    {
      fprintf(stderr, "error: time specified is equal to or after end time of recording\n");
      return -4;
    }

    chan = atoi(a->op_arg[2]);
    if((chan < 1) || (chan > hdr->total_chans))
    {
      fprintf(stderr, "error: channel %i does not exist in file\n", chan);
      err = -9;
      goto DUMP_OUT_EXIT;
    }
    chan--;

    use_chan = 1;

    time_based_smpls = atoi(a->op_arg[3]);
    if(time_based_smpls < 1)
    {
      fprintf(stderr, "error: argument samples must be > 0\n");
      err = -10;
      goto DUMP_OUT_EXIT;
    }

    if(dump_time_offset + (long long)((time_based_smpls * (double)hdr->datrec_duration / hdr->smpls_in_datrec[chan]) - (((double)hdr->datrec_duration / hdr->smpls_in_datrec[chan]) * 0.4))
        > hdr->datrecs * hdr->datrec_duration)
    {
      fprintf(stderr, "error: specified time + %i samples is after end time of recording\n", time_based_smpls);
      return -4;
    }

    datrec = dump_time_offset / hdr->datrec_duration;
    if(datrec >= hdr->datrecs)
    {
      fprintf(stderr, "internal error: datarecord number caculated from time based index does not exist in file: %i\n", datrec);
      err = -11;
      goto DUMP_OUT_EXIT;
    }

    time_based_datrecs = time_based_smpls / hdr->smpls_in_datrec[chan];
    if(time_based_smpls % hdr->smpls_in_datrec[chan])  time_based_datrecs++;

    time_based_datrec_offset = (dump_time_offset % hdr->datrec_duration) / ((double)hdr->datrec_duration / hdr->smpls_in_datrec[chan]);

    time_based_datrec_offset *= hdr->sample_width;

    // printf("dump_time_offset: %lli\n"
    //        "hdr->datrec_duration: %lli\n"
    //        "datrec: %i\n"
    //        "time_based_datrecs: %lli\n"
    //        "time_based_datrec_offset: %i\n",
    //        dump_time_offset,
    //        hdr->datrec_duration,
    //        datrec,
    //        time_based_datrecs,
    //        time_based_datrec_offset);  //FIXME
  }
  else  /* datarecord based index */
  {
    datrec = atoi(a->op_arg[1]);
    if((datrec < 1) || (datrec > hdr->datrecs))
    {
      fprintf(stderr, "error: datarecord number does not exist in file: %i\n", datrec);
      err = -8;
      goto DUMP_OUT_EXIT;
    }
    datrec--;

    if(a->op_arg_cnt > 2)
    {
      chan = atoi(a->op_arg[2]);
      if((chan < 1) || (chan > hdr->total_chans))
      {
        fprintf(stderr, "error: channel %i does not exist in file\n", chan);
        err = -9;
        goto DUMP_OUT_EXIT;
      }
      chan--;

      use_chan = 1;
    }

    if(a->op_arg_cnt > 3)
    {
      smpl = atoi(a->op_arg[3]);
      if((smpl < 1) || (smpl > hdr->bytes_in_datrec[chan] / hdr->sample_width))
      {
        fprintf(stderr, "error: sample %i of channel %i does not exist in datarecord\n", smpl, chan + 1);
        err = -10;
        goto DUMP_OUT_EXIT;
      }
      smpl--;

      use_smpl = 1;
    }
  }

  if(use_time_based_idx)
  {
    buf1 = malloc(time_based_datrecs * hdr->bytes_in_datrec[chan]);
    if(buf1 == NULL)
    {
      fprintf(stderr, "malloc error\n");
      err = -11;
      goto DUMP_OUT_EXIT;
    }

    for(int i=0; i<time_based_datrecs; i++)
    {
      err = get_datrec_data(a->in_fname[0], (void **)(&buf2), datrec + i, hdr);
      if(err)
      {
        fprintf(stderr, "error: get_datrec_data(): %i\n", err);
        err = -11;
        goto DUMP_OUT_EXIT;
      }

      memcpy(buf1 + (i * hdr->bytes_in_datrec[chan]), buf2 + hdr->datrec_offset[chan], hdr->bytes_in_datrec[chan]);

      free(buf2);
      buf2 = NULL;
    }
  }
  else  /* datarecord based index */
  {
    err = get_datrec_data(a->in_fname[0], (void **)(&buf1), datrec, hdr);
    if(err)
    {
      fprintf(stderr, "error: get_datrec_data(): %i\n", err);
      err = -11;
      goto DUMP_OUT_EXIT;
    }
  }

  if(mode == RAWMODE)
  {
    if(!use_chan)
    {
      fwrite(buf1, hdr->datrec_sz, 1, stdout);
    }
    else if(!use_smpl)
      {
        if(use_time_based_idx)
        {
          fwrite(buf1 + time_based_datrec_offset, time_based_smpls * hdr->sample_width, 1, stdout);
        }
        else
        {
          fwrite(buf1 + hdr->datrec_offset[chan], hdr->bytes_in_datrec[chan], 1, stdout);
        }
      }
      else
      {
        fwrite(buf1 + hdr->datrec_offset[chan] + (smpl * hdr->sample_width), hdr->sample_width, 1, stdout);
      }
  }
  else if(mode == HEXMODE)
    {
      if(!use_chan)
      {
        write_hex(buf1, hdr->datrec_sz, hdr->sample_width);
      }
      else if(!use_smpl)
        {
          if(use_time_based_idx)
          {
            write_hex(buf1 + time_based_datrec_offset, time_based_smpls * hdr->sample_width, hdr->sample_width);
          }
          else
          {
            write_hex(buf1 + hdr->datrec_offset[chan], hdr->bytes_in_datrec[chan], hdr->sample_width);
          }
        }
        else
        {
          write_hex(buf1 + hdr->datrec_offset[chan] + (smpl * hdr->sample_width), hdr->sample_width, hdr->sample_width);
        }
    }
    else if(mode == DECMODE)
      {
        if(!use_chan)
        {
          write_dec(buf1, hdr->datrec_sz, hdr->sample_width);
        }
        else if(!use_smpl)
          {
            if(use_time_based_idx)
            {
              write_dec(buf1 + time_based_datrec_offset, time_based_smpls * hdr->sample_width, hdr->sample_width);
            }
            else
            {
              write_dec(buf1 + hdr->datrec_offset[chan], hdr->bytes_in_datrec[chan], hdr->sample_width);
            }
          }
          else
          {
            write_dec(buf1 + hdr->datrec_offset[chan] + (smpl * hdr->sample_width), hdr->sample_width, hdr->sample_width);
          }
      }
      else if(mode == PHYSMODE)
        {
          if(!use_chan)
          {
            write_phys(buf1, hdr->datrec_sz, hdr->sample_width, hdr, -1);
          }
          else if(!use_smpl)
            {
              if(use_time_based_idx)
              {
                write_phys(buf1 + time_based_datrec_offset, time_based_smpls * hdr->sample_width, hdr->sample_width, hdr, chan);
              }
              else
              {
                write_phys(buf1 + hdr->datrec_offset[chan], hdr->bytes_in_datrec[chan], hdr->sample_width, hdr, chan);
              }
            }
            else
            {
              write_phys(buf1 + hdr->datrec_offset[chan] + (smpl * hdr->sample_width), hdr->sample_width, hdr->sample_width, hdr, chan);
            }
        }

  err = 0;

DUMP_OUT_EXIT:

  free(buf1);
  free(buf2);
  free(hdr);

  return err;
}


static int get_raw_header(const char *f_path, edfhdr_t *hdr)
{
  int i, err=0;

  char *buf=NULL,
       str[128]={""};

  FILE *f=NULL;

  if((f_path == NULL) || (hdr == NULL))  return -1;

  if(strlen(f_path) < 5)  return -2;

  memset(hdr, 0, sizeof(edfhdr_t));

  buf = malloc(300);
  if(buf==NULL)
  {
    err = -3;
    goto GRH_EXIT;
  }

  f = fopen(f_path, "rb");
  if(f==NULL)
  {
    perror(NULL);
    err = -4;
    goto GRH_EXIT;
  }

  if(fread(buf, 256, 1, f) != 1)
  {
    err = -11;
    goto GRH_EXIT;
  }

  if(!memcmp(buf, "0       ", 8))
  {
    hdr->sample_width = 2;
  }
  else if(!memcmp(buf, "\xff" "BIOSEMI", 8))
    {
      hdr->sample_width = 3;
    }
    else
    {
      err = -11;
      goto GRH_EXIT;
    }

  memcpy(str, buf+236, 8);
  str[8] = 0;
  hdr->datrecs = atoi(str);
  if(hdr->datrecs < 1)
  {
    err = -12;
    goto GRH_EXIT;
  }

  memcpy(str, buf+252, 4);
  str[4] = 0;
  hdr->total_chans = atoi(str);
  if((hdr->total_chans < 1) || (hdr->total_chans > EDFLIB_MAXSIGNALS))
  {
    err = -13;
    goto GRH_EXIT;
  }

  memcpy(str, buf+184, 8);
  str[8] = 0;
  hdr->hdr_sz = atoi(str);
  if((hdr->hdr_sz < 512) || (hdr->hdr_sz != ((hdr->total_chans + 1) * 256)))
  {
    err = -14;
    goto GRH_EXIT;
  }

  hdr->datrec_duration = edflib_get_long_duration(buf+244);
  if(hdr->datrec_duration < 1)
  {
    err = -15;
    goto GRH_EXIT;
  }

  free(buf);

  buf = malloc(hdr->hdr_sz);
  if(buf==NULL)
  {
    err = -16;
    goto GRH_EXIT;
  }

  rewind(f);

  if(fread(buf, hdr->hdr_sz, 1, f) != 1)
  {
    err = -17;
    goto GRH_EXIT;
  }

  for(i=0; i<hdr->total_chans; i++)
  {
    memcpy(str, buf+256+(hdr->total_chans*104)+(i*8), 8);
    str[8] = 0;
    hdr->physmin[i] = atof(str);

    memcpy(str, buf+256+(hdr->total_chans*112)+(i*8), 8);
    str[8] = 0;
    hdr->physmax[i] = atof(str);

    memcpy(str, buf+256+(hdr->total_chans*120)+(i*8), 8);
    str[8] = 0;
    hdr->digmin[i] = atoi(str);

    memcpy(str, buf+256+(hdr->total_chans*128)+(i*8), 8);
    str[8] = 0;
    hdr->digmax[i] = atoi(str);

    if((hdr->physmax[i] != hdr->physmin[i]) && (hdr->digmax[i] != hdr->digmin[i]))
    {
      hdr->phys_resolution[i] = (hdr->physmax[i] - hdr->physmin[i]) / (hdr->digmax[i] - hdr->digmin[i]);

      if(hdr->phys_resolution[i] < 1e-12)  hdr->phys_resolution[i] = 1e-12;
    }
    else
    {
      hdr->phys_resolution[i] = 1;
    }

    hdr->phys_offset[i] = (hdr->digmax[i] * hdr->phys_resolution[i]) - hdr->physmax[i];

    memcpy(str, buf+256+(hdr->total_chans*216)+(i*8), 8);
    str[8] = 0;
    hdr->smpls_in_datrec[i] = atoi(str);

    hdr->bytes_in_datrec[i] = hdr->smpls_in_datrec[i] * hdr->sample_width;

    hdr->datrec_offset[i] = hdr->datrec_sz;

    hdr->datrec_sz += hdr->bytes_in_datrec[i];

    if(hdr->sample_width == 2)
    {
      if(memcmp(buf+256+(i*16), "EDF Annotations ", 16))
      {
        hdr->regular_chans_idx_list[hdr->regular_chans] = i;

        hdr->regular_chans++;
      }
      else
      {
        hdr->annot_chans_idx_list[hdr->annot_chans] = i;

        hdr->annot_chans++;

        hdr->is_annot_ch[i] = 1;
      }
    }
    else
    {
      if(memcmp(buf+256+(i*16), "BDF Annotations ", 16))
      {
        hdr->regular_chans_idx_list[hdr->regular_chans] = i;

        hdr->regular_chans++;
      }
      else
      {
        hdr->annot_chans_idx_list[hdr->annot_chans] = i;

        hdr->annot_chans++;

        hdr->is_annot_ch[i] = 1;
      }
    }
  }

GRH_EXIT:

  if(f)  fclose(f);

  free(buf);

  if(err)  memset(hdr, 0, sizeof(edfhdr_t));

  return err;
}

/* datrec and chan are zero based
 * type: 0: regular channel 1: annotation channel  2: don't care
 */
static long long get_file_offset(int datrec, int chan, int type, edfhdr_t *hdr)
{
  if((datrec < 0) || (datrec >= hdr->datrecs))  return -1;

  if(!hdr)  return -2;

  if(type == 0)
  {
    if((chan < 0) || (chan >= hdr->regular_chans))  return -5;

    return (hdr->hdr_sz + (((long long)datrec) * hdr->datrec_sz) + (hdr->datrec_offset[hdr->regular_chans_idx_list[chan]]));
  }
  else if(type == 1)
    {
      if((chan < 0) || (chan >= hdr->annot_chans))  return -4;

      return (hdr->hdr_sz + (((long long)datrec) * hdr->datrec_sz) + (hdr->datrec_offset[hdr->annot_chans_idx_list[chan]]));
    }
    else if(type == 2)
      {
        if((chan < 0) || (chan >= hdr->total_chans))  return -5;

        return (hdr->hdr_sz + (((long long)datrec) * hdr->datrec_sz) + hdr->datrec_offset[chan]);
      }
      else
      {
        return 3;
      }
}


static int get_datrec_data(const char *path, void **buf, int datrec, edfhdr_t *hdr)
{
  int err=0;

  FILE *f=NULL;

  long long f_offset = get_file_offset(datrec, 0, 2, hdr);
  if(f_offset < 0)
  {
    fprintf(stderr, "error: get_datrec_data(): get_file_offset: %lli\n", f_offset);
    err = -1;
    goto GDRD_OUT_EXIT;
  }

  *buf = malloc(hdr->datrec_sz);
  if(*buf == NULL)
  {
    fprintf(stderr, "get_datrec_data(): malloc error\n");
    err = -2;
    goto GDRD_OUT_EXIT;
  }

  f = fopen(path, "rb");
  if(f == NULL)
  {
    perror("get_datrec_data(): cannot open file\n");
    err = -3;
    goto GDRD_OUT_EXIT;
  }

  if(fseek(f, f_offset, SEEK_SET))
  {
    perror("get_datrec_data(): cannot set file position\n");
    err = -4;
    goto GDRD_OUT_EXIT;
  }

  if(fread(*buf, hdr->datrec_sz, 1, f) != 1)
  {
    perror("get_datrec_data(): cannot read from file\n");
    err = -5;
    goto GDRD_OUT_EXIT;
  }

GDRD_OUT_EXIT:

  if(f != NULL)  fclose(f);

  if(err)
  {
    free(*buf);
    *buf = NULL;
  }

  return err;
}


static void write_hex(void *buf, int sz, int smpl_width)
{
  int left;

  char c;

  int smpls = sz / smpl_width;

  for(int s=0; s<smpls; s++)
  {
    if(!(s % 8))  printf("%08x ", s);

    if(!(s % 4))  printf(" ");

    if(smpl_width == 2)
    {
      printf("%04x ", ((unsigned short *)buf)[s]);
    }
    else
    {
      for(int r=2; r>=0; r--)
      {
        printf("%02x", *((unsigned char *)buf + (s * 3) + r));
      }

      printf(" ");
    }

    if(!((s + 1) % 8) && s)
    {
      printf(" |");

      for(int i=8*smpl_width; i>0; i--)
      {
        c = *((char *)(buf + ((s + 1) * smpl_width) - i));

        if(isprint(c))
          printf("%c", c);
        else
          printf(".");
      }

      printf("|\n");
    }
  }

  left = smpls % 8;

  if(left)
  {
    for(int i=0; i<(8-left)*(smpl_width*2+1); i++)
    {
      printf(" ");
    }

    if(left > 4)
      printf(" |");
    else
      printf("  |");

    for(int i=left*smpl_width; i>0; i--)
    {
      c = *((char *)(buf + sz - i));

      if(isprint(c))
        printf("%c", c);
      else
        printf(".");
    }

    printf("|\n");
  }
  else
  {
    printf("\n");
  }
}


static void write_dec(void *buf, int sz, int smpl_width)
{
  int left, val;

  char c;

  int smpls = sz / smpl_width;

  for(int s=0; s<smpls; s++)
  {
    if(!(s % 8))  printf("%010i ", s);

    if(!(s % 4))  printf(" ");

    if(smpl_width == 2)
    {
      printf("%+06i ", ((signed short *)buf)[s]);
    }
    else
    {
      val = *((unsigned char *)buf + (s * 3) + 2) << 24;

      val += *((unsigned short *)(buf + (s * 3))) << 8;

      printf("%+08i ", val >> 8);
    }

    if(!((s + 1) % 8) && s)
    {
      printf(" |");

      for(int i=8*smpl_width; i>0; i--)
      {
        c = *((char *)(buf + ((s + 1) * smpl_width) - i));

        if(isprint(c))
          printf("%c", c);
        else
          printf(".");
      }

      printf("|\n");
    }
  }

  left = smpls % 8;

  if(left)
  {
    if(smpl_width == 2)
    {
      for(int i=0; i<(8-left)*7; i++)
      {
        printf(" ");
      }
    }
    else
    {
      for(int i=0; i<(8-left)*9; i++)
      {
        printf(" ");
      }
    }

    if(left > 4)
      printf(" |");
    else
      printf("  |");

    for(int i=left*smpl_width; i>0; i--)
    {
      c = *((char *)(buf + sz - i));

      if(isprint(c))
        printf("%c", c);
      else
        printf(".");
    }

    printf("|\n");
  }
  else
  {
    printf("\n");
  }
}


static void write_phys(void *buf, int sz, int smpl_width, edfhdr_t *hdr, int chan)
{
  int left, val, chan_idx=0, chan_smpl_cnt=0;

  char c;

  int smpls = sz / smpl_width;

  if(chan >= 0)  chan_idx = chan;

  for(int s=0; s<smpls; s++, chan_smpl_cnt++)
  {
    if(chan == -1)
    {
      if(chan_smpl_cnt == hdr->smpls_in_datrec[chan_idx])
      {
        chan_smpl_cnt = 0;

        chan_idx++;
      }
    }

    if(!(s % 8))  printf("%010i ", s);

    if(!(s % 4))  printf(" ");

    if(smpl_width == 2)
    {
      printf("%+.3e ", ((signed short *)buf)[s] * hdr->phys_resolution[chan_idx]);
    }
    else
    {
      val = *((unsigned char *)buf + (s * 3) + 2) << 24;

      val += *((unsigned short *)(buf + (s * 3))) << 8;

      printf("%+.3e ", (val >> 8) * hdr->phys_resolution[chan_idx]);
    }

    if(!((s + 1) % 8) && s)
    {
      printf(" |");

      for(int i=8*smpl_width; i>0; i--)
      {
        c = *((char *)(buf + ((s + 1) * smpl_width) - i));

        if(isprint(c))
          printf("%c", c);
        else
          printf(".");
      }

      printf("|\n");
    }
  }

  left = smpls % 8;

  if(left)
  {
    for(int i=0; i<(8-left)*11; i++)
    {
      printf(" ");
    }

    if(left > 4)
      printf(" |");
    else
      printf("  |");

    for(int i=left*smpl_width; i>0; i--)
    {
      c = *((char *)(buf + sz - i));

      if(isprint(c))
        printf("%c", c);
      else
        printf(".");
    }

    printf("|\n");
  }
  else
  {
    printf("\n");
  }
}


















