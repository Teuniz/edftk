
#ifndef EDFTK_GLOBAL_H
#define EDFTK_GLOBAL_H

#define PROGRAM_NAME              "edftk"
#define PROGRAM_VERSION            "1.03"

#define MAX_PATH_LEN               (1024)
#define MAX_ALIAS_LEN                (32)
#define MAX_INPUT_FILES              (63)
#define MAX_OP_ARGS                  (32)
#define TIMEGAP_ERR_TRHESHOLD  (3600 * 2)


#include "edflib.h"


typedef struct
{
  int in_f_cnt;
  char const *in_fname[MAX_INPUT_FILES];
  char const *op;
  int op_arg_cnt;
  char const *op_arg[MAX_OP_ARGS];
  char const *out_fname;
} prg_args_t;


typedef struct
{
  int in_f_cnt;
  char in_fname[MAX_INPUT_FILES][MAX_PATH_LEN];
  char alias_fname[MAX_INPUT_FILES][MAX_ALIAS_LEN + 1];
  edflib_hdr_t hdr_in[MAX_INPUT_FILES];
  int hdl_in[MAX_INPUT_FILES];
  long long starttime_utc[MAX_INPUT_FILES];
  long long endtime_utc[MAX_INPUT_FILES];
  int annot_ch_cnt[MAX_INPUT_FILES];
  int annot_ch_idx[MAX_INPUT_FILES][EDFLIB_MAXSIGNALS];
  int annot_ch_spr[MAX_INPUT_FILES][EDFLIB_MAXSIGNALS];
  int sig_sel[MAX_INPUT_FILES][EDFLIB_MAXSIGNALS];
  int sig_idx[MAX_INPUT_FILES][EDFLIB_MAXSIGNALS];
  int sig_out[MAX_INPUT_FILES];
  char const *out_fname;
  int hdl_out;
  int num_sig_out;
  long long timegap_err_trh;
  int *buf_in;
  int *buf_out;
  int buf_total_smpl_sz;
  int buf_offset[EDFLIB_MAXSIGNALS];
  int buf_file_offset[MAX_INPUT_FILES];
  int buf_file_smpls[MAX_INPUT_FILES];
  long long datrec_duration;
  long long datrecs_out;
  long long rec_ss_start;
  long long rec_max_len;
  long long rec_datrecs_start;
  long long rec_datrecs_dur_lim;
} f_prop_t;

#endif



