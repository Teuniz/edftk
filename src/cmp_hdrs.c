/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"
#include "cmp_hdrs.h"


int cmp_hdrs(f_prop_t *prop)
{
  for(int i=1; i<prop->in_f_cnt; i++)
  {
    if(prop->hdr_in[0].filetype != prop->hdr_in[i].filetype)
    {
      fprintf(stderr, "error: inputfiles have different types (EDF/EDF+/BDF/BDF+)\n");
      return -1;
    }

    if(prop->hdr_in[0].edfsignals != prop->hdr_in[i].edfsignals)
    {
      fprintf(stderr, "error: inputfiles have different number of signals\n");
      return -2;
    }

    if(prop->hdr_in[0].datarecord_duration != prop->hdr_in[i].datarecord_duration)
    {
      fprintf(stderr, "error: inputfiles have different datarecord durations\n");
      return -3;
    }

    if(strcmp(prop->hdr_in[0].patient_name, prop->hdr_in[i].patient_name))
    {
      fprintf(stderr, "error: inputfiles have different subject names\n");
      return -4;
    }

    if(strcmp(prop->hdr_in[0].sex, prop->hdr_in[i].sex))
    {
      fprintf(stderr, "error: inputfiles have different sex for subject\n");
      return -5;
    }

    if((prop->hdr_in[0].birthdate_day != prop->hdr_in[i].birthdate_day) ||
       (prop->hdr_in[0].birthdate_month != prop->hdr_in[i].birthdate_month) ||
       (prop->hdr_in[0].birthdate_year != prop->hdr_in[i].birthdate_year))
    {
      fprintf(stderr, "error: inputfiles have different subject birthdates\n");
      return -6;
    }

    if(strcmp(prop->hdr_in[0].equipment, prop->hdr_in[i].equipment))
    {
      fprintf(stderr, "error: inputfiles have different 'equipment' descriptions\n");
      return -7;
    }

    for(int j=0; j<prop->hdr_in[0].edfsignals; j++)
    {
      if(strcmp(prop->hdr_in[0].signalparam[j].label, prop->hdr_in[i].signalparam[j].label))
      {
        fprintf(stderr, "error: inputfiles have different labels for signal %i\n", j + 1);
        return -51;
      }

      if(prop->hdr_in[0].signalparam[j].dig_max != prop->hdr_in[i].signalparam[j].dig_max)
      {
        fprintf(stderr, "error: inputfiles have different digital maximum for signal %i\n", j + 1);
        return -52;
      }

      if(prop->hdr_in[0].signalparam[j].dig_min != prop->hdr_in[i].signalparam[j].dig_min)
      {
        fprintf(stderr, "error: inputfiles have different digital minimum for signal %i\n", j + 1);
        return -53;
      }

      if(dblcmp(prop->hdr_in[0].signalparam[j].phys_max, prop->hdr_in[i].signalparam[j].phys_max))
      {
        fprintf(stderr, "error: inputfiles have different physical maximum for signal %i\n", j + 1);
        return -54;
      }

      if(dblcmp(prop->hdr_in[0].signalparam[j].phys_min, prop->hdr_in[i].signalparam[j].phys_min))
      {
        fprintf(stderr, "error: inputfiles have different physical minimum for signal %i\n", j + 1);
        return -55;
      }

      if(prop->hdr_in[0].signalparam[j].smp_in_datarecord != prop->hdr_in[i].signalparam[j].smp_in_datarecord)
      {
        fprintf(stderr, "error: inputfiles have different number of samples in datarecord for signal %i\n", j + 1);
        return -56;
      }

      if(strcmp(prop->hdr_in[0].signalparam[j].physdimension, prop->hdr_in[i].signalparam[j].physdimension))
      {
        fprintf(stderr, "error: inputfiles have different physical dimensions (unit) for signal %i\n", j + 1);
        return -57;
      }

      if(strcmp(prop->hdr_in[0].signalparam[j].prefilter, prop->hdr_in[i].signalparam[j].prefilter))
      {
        fprintf(stderr, "error: inputfiles have different prefilters for signal %i\n", j + 1);
        return -58;
      }

      if(strcmp(prop->hdr_in[0].signalparam[j].transducer, prop->hdr_in[i].signalparam[j].transducer))
      {
        fprintf(stderr, "error: inputfiles have different transducers for signal %i\n", j + 1);
        return -59;
      }
    }

    if(prop->annot_ch_cnt[0] != prop->annot_ch_cnt[i])
    {
      fprintf(stderr, "error: inputfiles have different number of annotation channels\n");
      return -61;
    }

    for(int r=0; r<prop->annot_ch_cnt[0] ; r++)
    {
      if(prop->annot_ch_idx[0][r] != prop->annot_ch_idx[i][r])
      {
        fprintf(stderr, "error: inputfiles have different indexes for annotation channels\n");
        return -62;
      }

      if(prop->annot_ch_spr[0][r] != prop->annot_ch_spr[i][r])
      {
        fprintf(stderr, "error: inputfiles have different number of samples in datarecord for annotation channels\n");
        return -63;
      }
    }
  }

  return 0;
}





