/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>

#include "utils.h"
#include "edflib.h"
#include "annotations.h"

extern volatile sig_atomic_t sig_flag;


int copy_annotations(f_prop_t *p)
{
  edflib_annotation_t annot;

  long long utc_start=INT64_MAX;

  for(int f=0; f<p->in_f_cnt; f++)
  {
    if(utc_start > p->starttime_utc[f])  utc_start = p->starttime_utc[f];
  }

  for(int f=0; f<p->in_f_cnt; f++)
  {
    for(int annot_idx=0; annot_idx<p->hdr_in[f].annotations_in_file; annot_idx++)
    {
      if(sig_flag)
      {
        fprintf(stderr, "\naborted\n");
        return -1;
      }

      if(edf_get_annotation(p->hdl_in[f], annot_idx, &annot))
      {
        fprintf(stderr, "error: edf_get_annotation()\n");
        return -2;
      }

      annot.onset += p->starttime_utc[f] - utc_start - (p->rec_datrecs_start * p->datrec_duration);
      if(annot.onset < 0)  continue;

      annot.onset /= 10;

      if(annot.duration_l > 0)
      {
        annot.duration_l /= 10;
      }
      else
      {
        annot.duration_l = -1;
      }

      if(edfwrite_annotation_utf8_hr(p->hdl_out, annot.onset, annot.duration_l, annot.annotation))
      {
        fprintf(stderr, "error: edfwrite_annotation_utf8_hr()\n");
      }
    }
  }

  return 0;
}


int set_annot_chns(f_prop_t *p)
{
  int num_annots=0,
      num_annot_chns;

  for(int f=0; f<p->in_f_cnt; f++)
  {
    num_annots += p->hdr_in[f].annotations_in_file;
  }

  num_annot_chns = num_annots / p->datrecs_out + 1;
  if(num_annot_chns > 64)  num_annot_chns = 64;

  if(num_annot_chns > 1)
  {
    if(edf_set_number_of_annotation_signals(p->hdl_out, num_annot_chns))
    {
      fprintf(stderr, "error: edf_set_number_of_annotation_signals()\n");
      return -2;
    }
  }

  return 0;
}










