/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <limits.h>

#include "utils.h"
#include "merge.h"
#include "edflib.h"
#include "annotations.h"
#include "utc_date_time.h"


extern volatile sig_atomic_t sig_flag;


typedef struct
{
  long long smpl_start[EDFLIB_MAXSIGNALS];
  long long num_smpls_dat[EDFLIB_MAXSIGNALS];
  long long smpl_end[EDFLIB_MAXSIGNALS];
  long long datrec_idx_start[EDFLIB_MAXSIGNALS];
  int smp_in_datarecord[EDFLIB_MAXSIGNALS];
  int datrec_pre_dat;
  long long num_datrecs_dat;
  long long num_datrecs_zero;
  long long datrec_seq_start;
  long long datrec_seq_end;
  int datrec_multi;
} out_layout_t;


static int init_edf_hdr(f_prop_t *, out_layout_t *);
static int init_smpl_ranges(f_prop_t *, out_layout_t *);
static void tidy_up(f_prop_t *, out_layout_t *);
static int check_samplerate_compatibility(f_prop_t *);
static int copy_smpl_data(f_prop_t *, out_layout_t *);
static int check_signalrange_arg(const char *);
static int parse_signalrange(prg_args_t *, f_prop_t *, int, int *, int *, int *);


int merge_files(prg_args_t *a, f_prop_t *p)
{
  int i, j, n,
      f_sel=-1,
      sig_idx_min=0,
      sig_idx_max=0,
      signal_range_set=0;

  out_layout_t *ol=NULL;

  if(a->out_fname == NULL)
  {
    fprintf(stderr, "error: missing argument output file\n");
    return -1;
  }

  for(i=0; i<a->op_arg_cnt; i++)
  {
    if(strlen(a->op_arg[i]) > 6 && !strncmp(a->op_arg[i], "start=", 6))
    {
      p->rec_ss_start = hhmmsstosec(a->op_arg[i] + 6);

      if(p->rec_ss_start < 0)
      {
        fprintf(stderr, "error: unrecognized or invalid operation argument: %s\n", a->op_arg[i]);
        return -2;
      }
    }
    else if(strlen(a->op_arg[i]) > 9 && !strncmp(a->op_arg[i], "duration=", 9))
      {
        p->rec_max_len = hhmmsstosec(a->op_arg[i] + 9);

        if(p->rec_max_len < 0)
        {
          fprintf(stderr, "error: unrecognized or invalid operation argument: %s\n", a->op_arg[i]);
          return -2;
        }
        else if(p->rec_max_len < 5)
          {
            fprintf(stderr, "error:  value of operation argument is too small: %s\n", a->op_arg[i]);
            return -3;
          }
      }
      else if(check_signalrange_arg(a->op_arg[i]))
        {
          fprintf(stderr, "error: unrecognized or invalid operation argument: %s\n", a->op_arg[i]);
          return -4;
        }
  }

  for(i=0; i<(p->in_f_cnt-1); i++)
  {
    for(j=i+1; j<p->in_f_cnt; j++)
    {
      if(p->endtime_utc[i] <= p->starttime_utc[j])
      {
        fprintf(stderr, "error: input files do not overlap in time,\n"
          "end time of file %s is before start time of file %s\n",
        p->in_fname[i], p->in_fname[j]);
        return -5;
      }

      if((p->endtime_utc[i] - (p->starttime_utc[j]) / EDFLIB_TIME_DIMENSION) < 1)
      {
        fprintf(stderr, "error: overlap of file %s and file %s is less than one second\n",
        p->in_fname[i], p->in_fname[j]);
        return -6;
      }
    }
  }

  if(a->op_arg_cnt)
  {
    for(n=0; n<a->op_arg_cnt; n++)
    {
      if((!strncmp(a->op_arg[n], "duration=", 9)) || (!strncmp(a->op_arg[n], "start=", 6)))  continue;

      if(parse_signalrange(a, p, n, &sig_idx_min, &sig_idx_max, &f_sel))
      {
        fprintf(stderr, "error: parsing signal range: %s\n", a->op_arg[i]);
        return -7;
      }

      if((sig_idx_min < 1) || (sig_idx_max > p->hdr_in[f_sel].edfsignals))
      {
        fprintf(stderr, "error: operation argument is out of range: %s\n", a->op_arg[i]);
        return -8;
      }

      sig_idx_min--;
      sig_idx_max--;

      for(i=0; i<p->hdr_in[f_sel].edfsignals; i++)
      {
        if((i >= sig_idx_min) && (i <= sig_idx_max))
        {
          p->sig_sel[f_sel][i] = 1;
        }
      }

      signal_range_set = 1;
    }
  }

  if(!signal_range_set)
  {
    for(j=0; j<p->in_f_cnt; j++)
    {
      for(i=0; i<p->hdr_in[j].edfsignals; i++)
      {
        p->sig_sel[j][i] = 1;
        p->sig_idx[j][i] = i;
      }
    }
  }

  for(j=0, p->num_sig_out=0; j<p->in_f_cnt; j++)
  {
    p->sig_out[j] = 0;

    for(i=0; i<p->hdr_in[j].edfsignals; i++)
    {
      if(p->sig_sel[j][i])
      {
        p->sig_idx[j][p->sig_out[j]] = i;

        p->sig_out[j]++;

        p->num_sig_out++;
      }
    }
  }

  p->out_fname = a->out_fname;

  if(check_samplerate_compatibility(p))
  {
    fprintf(stderr, "error: cannot create an output file that can accomodate for the\n"
                    "different samplerates used in the input files\n");
    goto MERGE_OUT_ERROR;
  }

  ol = (out_layout_t *)calloc(1, sizeof(out_layout_t[MAX_INPUT_FILES]));
  if(ol == NULL)
  {
    fprintf(stderr, "malloc error: init_smpl_ranges()\n");
    goto MERGE_OUT_ERROR;
  }

  if(init_smpl_ranges(p, ol))
  {
    goto MERGE_OUT_ERROR;
  }

  if(init_edf_hdr(p, ol))
  {
    edfclose_file(p->hdl_out);
    p->hdl_out = -1;
    fprintf(stderr, "error: cannot initialize header of output file\n");
    goto MERGE_OUT_ERROR;
  }

  if(set_annot_chns(p))
  {
    edfclose_file(p->hdl_out);
    p->hdl_out = -1;
    fprintf(stderr, "error: cannot initialize annotation channels of output file\n");
    goto MERGE_OUT_ERROR;
  }

  p->buf_in = (int *)malloc(sizeof(int) * p->buf_total_smpl_sz);
  if(p->buf_in == NULL)
  {
    fprintf(stderr, "error: malloc()\n");
    goto MERGE_OUT_ERROR;
  }

  if(copy_smpl_data(p, ol))
  {
    if(!sig_flag)  fprintf(stderr, "error: copy_smpl_data\n");
    goto MERGE_OUT_ERROR;
  }

  copy_annotations(p);

  tidy_up(p, ol);
  return 0;

MERGE_OUT_ERROR:

  tidy_up(p, ol);
  return -99;
}


static void tidy_up(f_prop_t *p, out_layout_t *ol)
{
  if(p->hdl_out >= 0)
  {
    edfclose_file(p->hdl_out);
    p->hdl_out = -1;
  }

  free(p->buf_in);
  p->buf_in = NULL;
  free(p->buf_out);
  p->buf_out = NULL;
  free(ol);
  ol = NULL;
}


static int init_edf_hdr(f_prop_t *p, out_layout_t *ol)
{
  int i, f_idx,
      isbdf=0,
      sig_out=0,
      sig_in=-1;

  long long starttime_utc;

  char *cp=NULL,
       path[MAX_PATH_LEN]={""};

  date_time_t dt;

  for(int f=0; f<p->in_f_cnt; f++)
  {
    if((p->hdr_in[f].filetype == EDFLIB_FILETYPE_BDF) || (p->hdr_in[f].filetype == EDFLIB_FILETYPE_BDFPLUS))
    {
      isbdf = 1;
      break;
    }
  }

  strlcpy(path, p->out_fname, MAX_PATH_LEN);

  cp = strrchr(path, '.');
  if(cp == NULL)
  {
    fprintf(stderr, "internal error, line %i  file %s\n", __LINE__, __FILE__);
    return -91;
  }

  if(isbdf)
  {
    if(strcmp(cp, ".bdf") && strcmp(cp, ".BDF"))
    {
      strcpy(cp, ".bdf");
    }

    p->hdl_out = edfopen_file_writeonly(path, EDFLIB_FILETYPE_BDFPLUS, p->num_sig_out);
  }
  else
  {
    if(strcmp(cp, ".edf") && strcmp(cp, ".EDF"))
    {
      strcpy(cp, ".edf");
    }

    p->hdl_out = edfopen_file_writeonly(path, EDFLIB_FILETYPE_EDFPLUS, p->num_sig_out);
  }
  if(p->hdl_out < 0)
  {
    fprintf(stderr, "error while opening output file: EDFlib: ");

    switch(p->hdl_out)
    {
      case EDFLIB_MALLOC_ERROR              : fprintf(stderr, "malloc error\n");
                                              break;
      case EDFLIB_NO_SUCH_FILE_OR_DIRECTORY : fprintf(stderr, "no such file or directory\n");
                                              break;
      case EDFLIB_MAXFILES_REACHED          : fprintf(stderr, "maximum number of files reached\n");
                                              break;
      case EDFLIB_FILE_ALREADY_OPENED       : fprintf(stderr, "file already opened\n");
                                              break;
      case EDFLIB_NUMBER_OF_SIGNALS_INVALID : fprintf(stderr, "number of signals invalid\n");
                                              break;
      case EDFLIB_ARCH_ERROR                : fprintf(stderr, "architectural error\n");
                                              break;
      default                               : fprintf(stderr, "unknown error\n");
                                              break;
    }

    return -1;
  }

  if(p->datrec_duration >= 100LL)
  {
    if(edf_set_datarecord_duration(p->hdl_out, p->datrec_duration / 100))
    {
      fprintf(stderr, "error: edf_set_datarecord_duration()\n");
      return -2;
    }
  }
  else if(p->datrec_duration >= 10LL)
    {
      if(edf_set_micro_datarecord_duration(p->hdl_out, p->datrec_duration / 10))
      {
        fprintf(stderr, "error: edf_set_micro_datarecord_duration()\n");
        return -3;
      }
    }
    else
    {
      fprintf(stderr, "error: datarecord duration of input file is too short\n");
      return -4;
    }

  f_idx = 0;

  for(int f=1; f<p->in_f_cnt; f++)
  {
    if(p->starttime_utc[f] < p->starttime_utc[f_idx])
    {
      f_idx = f;
    }
  }

  starttime_utc = p->starttime_utc[f_idx] + (p->rec_datrecs_start * p->datrec_duration);

  utc_to_date_time(starttime_utc / EDFLIB_TIME_DIMENSION, &dt);

  if(edf_set_startdatetime(p->hdl_out, dt.year, dt.month, dt.day,
                           dt.hour, dt.minute, dt.second))
  {
    fprintf(stderr, "error: edf_set_startdatetime()\n");
    return -5;
  }

  if(edf_set_subsecond_starttime(p->hdl_out, (int)(starttime_utc % EDFLIB_TIME_DIMENSION)))
  {
    fprintf(stderr, "error: edf_set_subsecond_starttime()\n");
    return -7;
  }

  if(edf_set_annot_chan_idx_pos(p->hdl_out, EDF_ANNOT_IDX_POS_START))
  {
    fprintf(stderr, "error: edf_set_annot_chan_idx_pos()\n");
    return -6;
  }

  if((p->hdr_in[0].filetype == EDFLIB_FILETYPE_EDF) || (p->hdr_in[0].filetype == EDFLIB_FILETYPE_BDF))
  {
    if(edf_set_patient_additional(p->hdl_out, p->hdr_in[0].patient))
    {
      fprintf(stderr, "error: edf_set_patient_additional()\n");
      return -8;
    }

    if(edf_set_recording_additional(p->hdl_out, p->hdr_in[0].recording))
    {
      fprintf(stderr, "error: edf_set_recording_additional()\n");
      return -9;
    }
  }
  else
  {
    if(edf_set_patientname(p->hdl_out, p->hdr_in[0].patient_name))
    {
      fprintf(stderr, "error: edf_set_patientname()\n");
      return -10;
    }

    if(!strcmp(p->hdr_in[0].sex, "Male"))
    {
      if(edf_set_sex(p->hdl_out, 1))
      {
        fprintf(stderr, "error: edf_set_sex()\n");
        return -11;
      }
    }
    else if(!strcmp(p->hdr_in[0].sex, "Female"))
      {
        if(edf_set_sex(p->hdl_out, 0))
        {
          fprintf(stderr, "error: edf_set_sex()\n");
          return -12;
        }
      }

    if(p->hdr_in[0].birthdate_day)
    {
      if(edf_set_birthdate(p->hdl_out, p->hdr_in[0].birthdate_year, p->hdr_in[0].birthdate_month, p->hdr_in[0].birthdate_day))
      {
        fprintf(stderr, "error: edf_set_birthdate()\n");
        return -13;
      }
    }

    if(edf_set_patient_additional(p->hdl_out, p->hdr_in[0].patient_additional))
    {
      fprintf(stderr, "error: edf_set_patient_additional()\n");
      return -14;
    }

    if(edf_set_admincode(p->hdl_out, p->hdr_in[0].admincode))
    {
      fprintf(stderr, "error: edf_set_admincode()\n");
      return -15;
    }

    if(edf_set_technician(p->hdl_out, p->hdr_in[0].technician))
    {
      fprintf(stderr, "error: edf_set_technician()\n");
      return -16;
    }

    if(edf_set_equipment(p->hdl_out, p->hdr_in[0].equipment))
    {
      fprintf(stderr, "error: edf_set_equipment()\n");
      return -17;
    }

    if(edf_set_recording_additional(p->hdl_out, p->hdr_in[0].recording_additional))
    {
      fprintf(stderr, "error: edf_set_recording_additional()\n");
      return -18;
    }
  }

  sig_out=0;

  for(int f=0; f<p->in_f_cnt; f++)
  {
    for(i=0; i<p->sig_out[f]; i++)
    {
      sig_in = p->sig_idx[f][i];

      if(edf_set_samplefrequency(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].smp_in_datarecord * ol[f].datrec_multi))
      {
        fprintf(stderr, "error: edf_set_samplefrequency()\n");
        return -51;
      }

      if(edf_set_digital_maximum(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].dig_max))
      {
        fprintf(stderr, "error: edf_set_digital_maximum()\n");
        return -52;
      }

      if(edf_set_digital_minimum(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].dig_min))
      {
        fprintf(stderr, "error: edf_set_digital_minimum()\n");
        return -53;
      }

      if(edf_set_physical_maximum(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].phys_max))
      {
        fprintf(stderr, "error: edf_set_physical_maximum()\n");
        return -54;
      }

      if(edf_set_physical_minimum(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].phys_min))
      {
        fprintf(stderr, "error: edf_set_physical_minimum()\n");
        return -55;
      }

      if(edf_set_label(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].label))
      {
        fprintf(stderr, "error: edf_set_label()\n");
        return -56;
      }

      if(edf_set_physical_dimension(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].physdimension))
      {
        fprintf(stderr, "error: edf_set_physical_dimension()\n");
        return -57;
      }

      if(edf_set_transducer(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].transducer))
      {
        fprintf(stderr, "error: edf_set_transducer()\n");
        return -58;
      }

      if(edf_set_prefilter(p->hdl_out, sig_out, p->hdr_in[f].signalparam[sig_in].prefilter))
      {
        fprintf(stderr, "error: edf_set_prefilter()\n");
        return -59;
      }

      sig_out++;
    }
  }

  return 0;
}


static int init_smpl_ranges(f_prop_t *p, out_layout_t *ol)
{
  long long t_start,
            utc_start=INT64_MAX,
            utc_end=0;

  double part;

  if(p->rec_ss_start)
  {
    p->rec_datrecs_start = (p->rec_ss_start * EDFLIB_TIME_DIMENSION) / p->datrec_duration;
  }

  if(p->rec_max_len)
  {
    p->rec_datrecs_dur_lim = p->rec_datrecs_start + ((p->rec_max_len * EDFLIB_TIME_DIMENSION) / p->datrec_duration);
  }
  else
  {
    p->rec_datrecs_dur_lim = LLONG_MAX;
  }

  for(int f=0; f<p->in_f_cnt; f++)
  {
    if(p->datrec_duration % p->hdr_in[f].datarecord_duration)
    {
      fprintf(stderr, "internal error: file: %s  line: %i  %s\n", __FILE__, __LINE__, __FUNCTION__);
      return -1;
    }

    ol[f].datrec_multi = p->datrec_duration / p->hdr_in[f].datarecord_duration;

    if(utc_start > p->starttime_utc[f])  utc_start = p->starttime_utc[f];

    if(utc_end < p->endtime_utc[f])  utc_end = p->endtime_utc[f];
  }

  p->datrecs_out = (utc_end - utc_start) / p->datrec_duration;

  if((utc_end - utc_start) % p->datrec_duration)  p->datrecs_out++;

  p->num_sig_out=0;

  p->buf_total_smpl_sz = 0;

  for(int f=0; f<p->in_f_cnt; f++)
  {
    t_start = p->starttime_utc[f] - utc_start;

    ol[f].datrec_seq_start = t_start / p->datrec_duration;

    part = (t_start % p->datrec_duration) / (double)p->datrec_duration;

    p->buf_file_offset[f] = p->buf_total_smpl_sz;

    p->buf_file_smpls[f] = 0;

    for(int i=0; i<p->sig_out[f]; i++)
    {
      if(p->num_sig_out == EDFLIB_MAXSIGNALS)
      {
        fprintf(stderr, "error: too many signals in all inputfiles together\n");
        return -2;
      }

      ol[f].smp_in_datarecord[i] = p->hdr_in[f].signalparam[p->sig_idx[f][i]].smp_in_datarecord * ol[f].datrec_multi;

      p->buf_offset[p->num_sig_out] = p->buf_total_smpl_sz;

      p->buf_total_smpl_sz += ol[f].smp_in_datarecord[i];

      p->buf_file_smpls[f] += ol[f].smp_in_datarecord[i];

      ol[f].smpl_start[i] = ol[f].datrec_seq_start * ol[f].smp_in_datarecord[i];

      ol[f].smpl_start[i] += part * ol[f].smp_in_datarecord[i];

      ol[f].num_smpls_dat[i] = p->hdr_in[f].signalparam[p->sig_idx[f][i]].smp_in_file;

      ol[f].smpl_end[i] = ol[f].smpl_start[i] + ol[f].num_smpls_dat[i];

      ol[f].datrec_idx_start[i] = ol[f].smpl_start[i] % ol[f].smp_in_datarecord[i];

      if(ol[f].datrec_idx_start[i])
      {
        ol[f].datrec_pre_dat = 1;
      }

      p->num_sig_out++;
    }

    ol[f].num_datrecs_dat = p->hdr_in[f].datarecords_in_file / ol[f].datrec_multi;

    if(p->hdr_in[f].datarecords_in_file % ol[f].datrec_multi)
    {
      ol[f].datrec_pre_dat = 1;
    }

    ol[f].datrec_seq_end = ol[f].datrec_seq_start + ol[f].num_datrecs_dat;
  }

  if(p->buf_total_smpl_sz > 5 * 1024 * 1024)
  {
    fprintf(stderr, "error: resulting datarecord size in output file would exceed 5 MegaSamples\n");
    return -3;
  }

  return 0;
}


static int check_samplerate_compatibility(f_prop_t *p)
{
  int f, is_int;

  p->datrec_duration = 0;

  for(f=0; f<p->in_f_cnt-1; f++)
  {
    if(p->hdr_in[f].datarecord_duration != p->hdr_in[f+1].datarecord_duration)
    {
      break;
    }
  }
  if(f == p->in_f_cnt - 1)
  {
    p->datrec_duration = p->hdr_in[0].datarecord_duration;

    goto CHCK_SR_COMPAT_OUT;
  }

  for(f=0, is_int=1; f<p->in_f_cnt; f++)
  {
    for(int i=0; i<p->hdr_in[f].edfsignals; i++)
    {
      if((p->hdr_in[f].signalparam[i].smp_in_datarecord * EDFLIB_TIME_DIMENSION) % p->hdr_in[f].datarecord_duration)
      {
        is_int = 0;

        break;
      }
    }

    if(!is_int) break;
  }
  if(is_int)
  {
    p->datrec_duration = EDFLIB_TIME_DIMENSION;

    goto CHCK_SR_COMPAT_OUT;
  }

  p->datrec_duration = p->hdr_in[0].datarecord_duration;

  for(f=1; f<p->in_f_cnt; f++)
  {
    if(p->hdr_in[f].datarecord_duration == p->datrec_duration)
    {
      continue;
    }

    if(p->hdr_in[f].datarecord_duration > p->datrec_duration)
    {
      if(p->hdr_in[f].datarecord_duration % p->datrec_duration)
      {
        if((p->hdr_in[f].datarecord_duration * 2) % p->datrec_duration)
        {
          if((p->hdr_in[f].datarecord_duration * 3) % p->datrec_duration)
          {
            p->datrec_duration = 0;

            return -1;
          }
          else
          {
            p->datrec_duration = p->hdr_in[f].datarecord_duration * 3;
          }
        }
        else
        {
          p->datrec_duration = p->hdr_in[f].datarecord_duration * 2;
        }
      }
      else
      {
        p->datrec_duration = p->hdr_in[f].datarecord_duration;
      }
    }
    else
    {
      if(p->datrec_duration % p->hdr_in[f].datarecord_duration)
      {
        if((p->datrec_duration * 2) % p->hdr_in[f].datarecord_duration)
        {
          if((p->datrec_duration * 3) % p->hdr_in[f].datarecord_duration)
          {
            p->datrec_duration = 0;

            return -2;
          }
          else
          {
            p->datrec_duration *= 3;
          }
        }
        else
        {
          p->datrec_duration *= 2;
        }
      }
    }
  }

CHCK_SR_COMPAT_OUT:

  return 0;
}


int copy_smpl_data(f_prop_t *p, out_layout_t *ol)
{
  int n,
      num_read,
      buf_offset,
      sig_out,
      sig_in;

  // for(int f=0; f<p->in_f_cnt; f++)  // FIXME
  // {
  //   for(int i=0; i<p->sig_out[f]; i++)  //FIXME
  //   {
  //     fprintf(stderr, "sig %i smp_in_datarecord: %i\n", i + 1, ol[f].smp_in_datarecord[i]);
  //     fprintf(stderr, "sig %i buf_offset: %i\n", i + 1, p->buf_offset[i]);
  //     fprintf(stderr, "sig %i datrec_idx_start: %lli\n", i + 1, ol[f].datrec_idx_start[i]);
  //     fprintf(stderr, "sig %i smpl_start: %lli\n", i + 1, ol[f].smpl_start[i]);
  //     fprintf(stderr, "sig %i smpl_end: %lli\n\n", i + 1, ol[f].smpl_end[i]);
  //   }
  //
  //   fprintf(stderr, "\nfile: %s\n", p->in_fname[f]);  //FIXME
  //   fprintf(stderr, "datrec_seq_start: %lli\n", ol[f].datrec_seq_start);
  //   fprintf(stderr, "datrec_seq_end: %lli\n", ol[f].datrec_seq_end);
  //   fprintf(stderr, "num_datrecs_dat %lli\n", ol[f].num_datrecs_dat);
  //   fprintf(stderr, "datrec_pre_dat: %i\n", ol[f].datrec_pre_dat);
  //   fprintf(stderr, "num_datrecs_zero: %lli\n", ol[f].num_datrecs_zero);
  //   fprintf(stderr, "datrec_multi: %i\n", ol[f].datrec_multi);  //FIXME
  //   fprintf(stderr, "buf_file_smpls: %i\n", p->buf_file_smpls[f]);
  //   fprintf(stderr, "buf_file_offset: %i\n\n", p->buf_file_offset[f]);
  // }
  //
  // fprintf(stderr, "\ndatrec_duration: %lli\n", p->datrec_duration);  //FIXME
  // fprintf(stderr, "datrecs_out: %lli\n\n", p->datrecs_out);  //FIXME
  //
  // fprintf(stderr, "\nrec_datrecs_start: %lli\n", p->rec_datrecs_start);  //FIXME
  // fprintf(stderr, "rec_datrecs_dur_lim: %lli\n\n", p->rec_datrecs_dur_lim);  //FIXME

  memset(p->buf_in, 0, sizeof(int) * p->buf_total_smpl_sz);

  for(long long datrec=0; datrec<p->datrecs_out; datrec++)
  {
    if(sig_flag || (datrec == p->rec_datrecs_dur_lim))  break;

    sig_out = 0;

    for(int f=0; f<p->in_f_cnt; f++)
    {
      if((datrec >= ol[f].datrec_seq_start && datrec < ol[f].datrec_seq_end) ||
         (datrec == ol[f].datrec_seq_end && ol[f].datrec_pre_dat) ||
         (datrec == ol[f].datrec_seq_start && datrec == ol[f].datrec_seq_end))
      {
        for(int i=0; i<p->sig_out[f]; i++)
        {
          sig_in = p->sig_idx[f][i];

          if(datrec == ol[f].datrec_seq_start && datrec == ol[f].datrec_seq_end)
          {
            num_read = ol[f].num_smpls_dat[i];

            buf_offset = p->buf_offset[sig_out] + ol[f].datrec_idx_start[i];
          }
          else if(datrec == ol[f].datrec_seq_start && ol[f].datrec_pre_dat)
            {
              num_read = ol[f].smp_in_datarecord[i] - ol[f].datrec_idx_start[i];

              buf_offset = p->buf_offset[sig_out] + ol[f].datrec_idx_start[i];
            }
            else if(datrec == ol[f].datrec_seq_end && ol[f].datrec_pre_dat)
              {
                if(!i)
                {
                  memset(&p->buf_in[p->buf_file_offset[f]], 0, p->buf_file_smpls[f] * sizeof(int));
                }

                num_read = ol[f].smpl_end[i] % ol[f].smp_in_datarecord[i];

                buf_offset = p->buf_offset[sig_out];
              }
              else
              {
                num_read = ol[f].smp_in_datarecord[i];

                buf_offset = p->buf_offset[sig_out];
              }

          n = edfread_digital_samples(p->hdl_in[f], sig_in, num_read, &p->buf_in[buf_offset]);
          if(n != num_read)
          {
            fprintf(stderr, "error: edfread_digital_samples(): cannot read %i samples of signal %i from file %s\n"
                            "    output datarecord: %lli  got %i smpls\n", num_read, i + 1, p->in_fname[f], datrec + 1, n);
            return -1;
          }

          sig_out++;
        }
      }
      else
      {
        if((datrec == ol[f].datrec_seq_end) || (datrec == ol[f].datrec_seq_end + 1))
        {
          memset(&p->buf_in[p->buf_file_offset[f]], 0, p->buf_file_smpls[f] * sizeof(int));
        }

        sig_out += p->sig_out[f];
      }
    }

    if(datrec >= p->rec_datrecs_start)
    {
      if(edf_blockwrite_digital_samples(p->hdl_out, p->buf_in))
      {
        fprintf(stderr, "error: edf_blockwrite_digital_samples() cannot write samples of datarecord %lli to file %s\n",
                datrec + 1, p->out_fname);
        return -3;
      }
    }
  }

  return 0;
}


/* Only returns 0 when the string argument contains
 * either: "A3", "BB5-18" or "CCC4-end". "A0" or '2' is not allowed.
 */
static int check_signalrange_arg(const char *s)
{
  int digit_b=0, digit_a=0, dash=0, end=0;

  if(!isupper(*s))  return -1;

  for(; *s != 0; s++)
  {
    if(!isupper(*s))
    {
      break;
    }
  }

  if(!atoi(s))  return -2;

  for(; *s != 0; s++)
  {
    if(isdigit(*s))
    {
      if(end)  return -3;

      if(dash)
      {
        if(!digit_a)
        {
          if(!atoi(s))  return -4;
        }
        digit_a++;
      }
      else
      {
        digit_b++;
      }
    }
    else if(*s == '-')
      {
        if(end || !digit_b || dash) return -5;
        dash++;
      }
      else if(!strcmp(s, "end"))
        {
          if(end || !dash || !digit_b || digit_a)  return -6;
          end++;
          s += 2;
        }
        else
        {
          return -7;
        }
  }

  if(!digit_b || (dash && !digit_a && !end)) return -8;

  return 0;
}


static int parse_signalrange(prg_args_t *a, f_prop_t *p, int idx, int *min, int *max, int *f_in)
{
  int tmp, prefix_len;

  const char *c_p=NULL;

  char alias[MAX_ALIAS_LEN + 1];

  for(prefix_len=0; prefix_len<MAX_ALIAS_LEN; prefix_len++)
  {
    alias[prefix_len] = a->op_arg[idx][prefix_len];

    if(!isupper(a->op_arg[idx][prefix_len]))
    {
      alias[prefix_len] = 0;

      break;
    }
  }

  for(int f=0; f<p->in_f_cnt; f++)
  {
    if(!strcmp(alias, p->alias_fname[f]))
    {
      *f_in = f;

      break;
    }
    else
    {
      if(f == p->in_f_cnt - 1)  return -1;
    }
  }

  *min = atoi(&a->op_arg[idx][prefix_len]);

  c_p = strstr(a->op_arg[idx], "-");
  if(c_p == NULL)
  {
    *max = *min;
  }
  else
  {
    c_p++;
    if(!strcmp(c_p, "end"))
    {
      *max = p->hdr_in[*f_in].edfsignals;
    }
    else
    {
      *max = atoi(c_p);
    }
  }

  if(*min > *max)
  {
    tmp = *min;
    *min = *max;
    *max = tmp;
  }

  return 0;
}











