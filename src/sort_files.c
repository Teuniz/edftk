/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"
#include "edflib.h"
#include "utc_date_time.h"
#include "sort_files.h"



void sort_input_files(f_prop_t *prop)
{
  int i, tmp;

  long long ltmp;

  char str_tmp[MAX_PATH_LEN]={""};

  edflib_hdr_t hdr;

  for(i=0; i<prop->in_f_cnt-1; i++)
  {
    if(prop->starttime_utc[i] > prop->starttime_utc[i+1])
    {
      tmp = prop->hdl_in[i];
      prop->hdl_in[i] = prop->hdl_in[i+1];
      prop->hdl_in[i+1] = tmp;

      hdr = prop->hdr_in[i];
      prop->hdr_in[i] = prop->hdr_in[i+1];
      prop->hdr_in[i+1] = hdr;

      strlcpy(str_tmp, prop->in_fname[i], MAX_PATH_LEN);
      strlcpy(prop->in_fname[i], prop->in_fname[i+1], MAX_PATH_LEN);
      strlcpy(prop->in_fname[i+1], str_tmp, MAX_PATH_LEN);

      ltmp = prop->starttime_utc[i];
      prop->starttime_utc[i] = prop->starttime_utc[i+1];
      prop->starttime_utc[i+1] = ltmp;

      ltmp = prop->endtime_utc[i];
      prop->endtime_utc[i] = prop->endtime_utc[i+1];
      prop->endtime_utc[i+1] = ltmp;

      if(i)
      {
        i -= 2;
      }
    }
  }
}













