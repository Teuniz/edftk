/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "global.h"
#include "help.h"


void print_help(int selection)
{
  if(selection == HELP_SYNOPSIS)
  {
    fprintf(stdout,
      "SYNOPSIS\n"
      "     " PROGRAM_NAME " <input EDF files>\n"
      "           <operation> [operation arguments]\n"
      "           [output <output filename>]\n"
      "     Where:\n"
      "          <operation> is:\n"
      "          [ list <option> | merge <option> | cat <option> | check | dump <option> ]\n"

      "\n    For complete help: " PROGRAM_NAME " --help\n\n");
  }

  if(selection == HELP_COMPLETE)
  {
    fprintf(stdout,
      "\nCopyright (C) 2024 Teunis van Beelen\n\n"

      "This program is free software: you can redistribute it and/or modify\n"
      "it under the terms of the GNU General Public License as published by\n"
      "the Free Software Foundation, version 3 of the License.\n"
      "This program is distributed in the hope that it will be useful,\n"
      "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
      "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
      "GNU General Public License for more details.\n"
      "You should have received a copy of the GNU General Public License\n"
      "along with this program.  If not, see <http://www.gnu.org/licenses/>.\n\n"

      "NAME\n"
      "     " PROGRAM_NAME " - A handy tool for manipulating EDF and BDF  version " PROGRAM_VERSION "\n\n"

      "SYNOPSIS\n"
      "     " PROGRAM_NAME " <input EDF files>\n"
      "           <operation> [operation arguments]\n"
      "           [output <output filename>]\n"
      "     Where:\n"
      "          <operation> is:\n"
      "          [ list <option> | merge <option> | cat <option> | check | dump <option> ]\n\n"

      "     list [ header | signals | annotation-channel | annotations | all ]\n"
      "          Prints the (general) header or the signal headers or the\n"
      "          annotation channels or the annotations (tab separated columns for\n"
      "          onset, duration and description) to stdout. The output option is not used.\n"
      "          Default (no operation arguments given) is to print the header and the signals.\n\n"

      "     merge [ signal ranges | start=<h:mm:ss> | duration=<h:mm:ss> ]\n"
      "          Merge input files. Recordings must overlap chronologically.\n"
      "          If signal ranges are used, they must be used for every input file.\n"
      "          Only when no signal ranges are used at all, all signals from all files will\n"
      "          be included automatically.\n"
      "          If signal ranges are used, a file handle must be used, where a handle\n"
      "          is one or more upper-case letters:\n\n"

      "          <input EDF handle>=<input EDF filename>\n\n"

      "          For example: A=input1.edf QT=input2.edf M=input3.edf\n\n"

      "          Page ranges are described like this:\n\n"

      "          <input EDF handle>[<begin page number>[-<end page number>]\n\n"

      "          Where the handle identifies one of the input EDF files, and\n"
      "          the beginning and ending signal numbers are one-based references\n"
      "          to signals in the EDF file.\n\n"

      "          When merging multiple files, the subject info and recording info will\n"
      "          be taken from the first input file argument.\n\n"

      "          start: start after time relative to the start of the input file with the earliest\n"
      "          starttime.\n"
      "          duration: maximum length (in time) of the output file.\n"
      "          For both options, the granularity depends on the internal datarecord duration\n"
      "          (often one second but can be different)\n\n"

      "          Input files maybe of different types (EDF/EDF+/BDF/BDF+) in which case the output file will\n"
      "          be of type BDF+. Only if all input files are of type EDF and/or EDF+, the output file\n"
      "          will be of type EDF+.\n"
      "          Old type EDF/BDF files are accepted as input files but output files are always EDF+ or BDF+\n"
      "          (which are backwards compatible with EDF/BDF).\n\n"

      "     cat [ signal ranges | maxtimegap=<h:mm:ss> ]\n"
      "          Export signals and/or catenates input files\n"
      "          Recordings must not overlap chronologically and must have the same\n"
      "          layout i.e. they must be identical (same number of signals, same signal labels,\n"
      "          same samplerates,etc.)\n\n"

      "     check\n"
      "          Check input files for compatibility.\n\n"

      "     dump < hex | dec | phys | raw > < time channel samples | datarecord  [ channel [ sample ] ] >\n"
      "          Write a datarecord to stdout.\n"
      "          hex:  hexadecimal\n"
      "          dec:  decimal\n"
      "          phys: physical (units e.g. uV)\n"
      "          raw: unmodified, only useful when piping to another tool or file.\n\n"

      "          Time based index:\n"
      "              The argument time indicates the sample at hh:mm:ss(.uu) since the start of the recording.\n"
      "              The argument channel is the one-based index number of a channel.\n"
      "              The argument samples specifies the number of samples to be dumped.\n\n"

      "          Datarecord based index:\n"
      "              The argument datarecord is the one-based index number of a datarecord.\n\n"

      "              The optional argument channel is the one-based index number of a channel in\n"
      "              which case only that channel of that datarecord will be written to sdtout.\n\n"

      "              The optional argument sample is the one-based index number of a sample in\n"
      "              which case only that sample of that channel of that datarecord will be written.\n"
      "              to stdout\n\n"

      "     [ output < output filename > ]\n"
      "          The output filename may not be set to the name of an input filename.\n"
      "          Not used with the list, check and dump operation.\n"
      "          If the output file already exists, it will be silently overwritten without advance warning.\n\n"

      "EXAMPLES\n"
      "      Print the general and signals header\n"
      "        '" PROGRAM_NAME " 1.edf list'\n"
      "        or\n"
      "        '" PROGRAM_NAME " 1.edf list all'\n"
      "        if you want also info about the annotation channels\n\n"

      "      Merge two or more files\n"
      "        '" PROGRAM_NAME " 1.edf 2.edf merge output 3.edf'\n"
      "        All signals of all input files will be copied into the output file.\n"
      "        '" PROGRAM_NAME " A=1.edf B=2.edf merge A1-4 B2-7 B12 output 3.edf'\n"
      "        Signal 1 to 4 from file 1.edf and signal 2 to 7 and 12 from file 2.edf\n"
      "        will be copied into the output file.\n\n"

      "      Export one or more signals into a new file\n"
      "        '" PROGRAM_NAME " 1.edf cat 3 8 9 10 output 2.edf'\n"
      "        or\n"
      "        '" PROGRAM_NAME " 1.edf cat 3 8-10 output 2.edf'\n"
      "        will export signals 3, 8, 9 and 10 from 1.edf to 2.edf.\n"

      "      Catenate two or more files\n"
      "        '" PROGRAM_NAME " 1.edf 2.edf cat output 3.edf'\n"
      "        Note: files must be of the same type (EDF(+)/BDF(+)) and share the same layout which means\n"
      "        they must have the same number of channels, same channel parameters, same values for patient info, etc.\n"
      "        Practically this means that the input files must have been generated by the same device and the same subject.\n"
      "        Recordings may not overlap in time. Timegaps between recordings will be filled with zeros/baseline.\n"
      "        Optionally specify which signals should be in the output file:\n"
      "        '" PROGRAM_NAME " 1.edf 2.edf cat 1-4 6-end output 3.edf' will catenate the input files without signal 5\n"
      "        Because the input files must have identical layouts, input file handles (like for the 'merge' operation)\n"
      "        are not used.\n\n"

      "        The maximum allowable time gap between end time and start time of consecutive files is 2 hours but can be\n"
      "        changed using the option 'maxtimegap=h:mm:ss'. The empty space will be filled with zeros/baseline values.\n\n"

      "      Check a file for compatibility\n"
      "        '" PROGRAM_NAME " 1.edf check'\n"
      "        In case of an error, open the file with EDFbrowser for more info about the error.\n\n"

      "      Debug a file using the dump operation\n"
      "        '" PROGRAM_NAME " 1.edf dump phys 0:00:13.7 2 4' prints 4 samples of channel 2 starting at time point 13.7 sec.\n"
      "        in physical (units) format.\n\n"

      "        '" PROGRAM_NAME " 1.edf dump hex 5' prints the content of the 5th datarecord in hexadecimal format.\n\n"

      "        '" PROGRAM_NAME " 1.edf dump dec 5 2' prints the content of the 2nd channel of 5th datarecord in\n"
      "        decimal format.\n\n"

      "        '" PROGRAM_NAME " 1.edf dump raw 5 | hexdump -C' prints the content of the 5th datarecord using the\n"
      "        traditional 'hexdump'.\n\n"

      "NOTES\n"
      "      Signals and annotation channels are not the same. Most channels in an EDF+ or BDF+ file\n"
      "      are signals which contain sampled waveforms. Annotation channels on the other hand contain\n"
      "      annotations, sometimes called events. They are simply timestamps with a string of text in UTF-8 format.\n"
      "      In an old type EDF file, the number of channels equals the number of signals.\n"
      "      In an EDF+ file the number of channels is always higher than the number of signals\n"
      "      because it contains one or more annotation channels. When selecting a signal (range),\n"
      "      you don't need to worry about the annotation channels, no matter the annotation channels\n"
      "      are located at the beginning, the end or in the middle of the list of channels, because\n"
      "      when indexing the signals, the annotation channels are ignored.\n\n"

      "      Difference between the merge and catenate operations:\n"
      "      When catenating files, the number of signals in the output file will be the same as in the input files\n"
      "      (assuming that no signal(range) has been selected). In other words, the signals themselves will be\n"
      "      extended (serialized).\n"
      "      When merging files, the number of signals in the output file will be the sum of all the signals in the\n"
      "      input files (parallelized).\n\n"

      "      The dump operation can be applied also to non-compliant files.\n\n"

      "      Except for the dump operation, discontinuous files (EDF+D/BDF+D) are not supported.\n\n"

      "SEE ALSO\n"
      "      EDFbrowser, EDF-resampler, EDF Header Repair, EDF de-identifier (anonymizer), EDF+D to EDF+C converter\n\n"

      "BUGS\n"
      "      It's not possible to change the order of the signals.\n\n"

      "AUTHOR\n"
      "      Teunis van Beelen  <teuniz@protonmail.com>\n\n"

      "COPYRIGHT\n"
      "      (c) 2024 Teunis van Beelen  <teuniz@protonmail.com>\n\n");
  }
}









