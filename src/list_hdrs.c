/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "utils.h"
#include "list_hdrs.h"

extern volatile sig_atomic_t sig_flag;


int list_headers(prg_args_t *args, f_prop_t *prop)
{
  int i, j, p,
      channel,
      show_main=0,
      show_signals=0,
      show_annot_chns=0,
      show_annotations=0;

  char str1[4096]={""},
       str2[1024]={""};

  const char f_type[][8]={"EDF", "EDF+", "BDF", "BDF+"};

  edflib_annotation_t annot;

  if(args->op_arg_cnt)
  {
    for(i=0; i<args->op_arg_cnt; i++)
    {
      if(!strcmp(args->op_arg[i], "header"))
      {
        show_main = 1;
      }
      else if(!strcmp(args->op_arg[i], "signals"))
        {
          show_signals = 1;
        }
        else if(!strcmp(args->op_arg[i], "annotation-channel"))
          {
            show_annot_chns = 1;
          }
          else if(!strcmp(args->op_arg[i], "annotations"))
            {
              show_annotations = 1;
            }
            else if(!strcmp(args->op_arg[i], "all"))
              {
                show_main = 1;
                show_signals = 1;
                show_annot_chns = 1;
                show_annotations = 1;
              }
              else
              {
                fprintf(stderr, "invalid operation argument: %s\n", args->op_arg[i]);
                return -1;
              }
    }
  }
  else
  {
    show_main = 1;
    show_signals = 1;
  }

  for(i=0; i<prop->in_f_cnt; i++)
  {
    if(show_main)
    {
      fprintf(stdout, "filetype: %s\n", f_type[prop->hdr_in[i].filetype]);
      fprintf(stdout, "signals: %i\n", prop->hdr_in[i].edfsignals);
      snprintf(str2, 1024, "file duration: %lli:%02lli:%02lli.%03lli\n",
               prop->hdr_in[i].file_duration / (EDFLIB_TIME_DIMENSION * 3600),
              (prop->hdr_in[i].file_duration / (EDFLIB_TIME_DIMENSION * 60)) % 60,
              (prop->hdr_in[i].file_duration / EDFLIB_TIME_DIMENSION) % 60,
              (prop->hdr_in[i].file_duration % EDFLIB_TIME_DIMENSION) / 10000);
      remove_trailing_zeros(str2);
      fprintf(stdout, "%s", str2);
      fprintf(stdout, "startdate: %i-%i-%i\n", prop->hdr_in[i].startdate_day, prop->hdr_in[i].startdate_month, prop->hdr_in[i].startdate_year);
      snprintf(str2, 1024, "starttime: %i:%02i:%02i.%03lli\n", prop->hdr_in[i].starttime_hour, prop->hdr_in[i].starttime_minute, prop->hdr_in[i].starttime_second, prop->hdr_in[i].starttime_subsecond / 10000);
      remove_trailing_zeros(str2);
      fprintf(stdout, "%s", str2);

      if((prop->hdr_in[i].filetype == 0) || (prop->hdr_in[i].filetype == 2))
      {
        fprintf(stdout, "patient: %s\n", prop->hdr_in[i].patient);
        fprintf(stdout, "recording: %s\n", prop->hdr_in[i].recording);
      }
      else
      {
        fprintf(stdout, "patientcode: %s\n", prop->hdr_in[i].patientcode);
        fprintf(stdout, "sex: %s\n", prop->hdr_in[i].sex);
        fprintf(stdout, "birthdate: %s\n", prop->hdr_in[i].birthdate);
        fprintf(stdout, "patient_name: %s\n", prop->hdr_in[i].patient_name);
        fprintf(stdout, "patient_additional: %s\n", prop->hdr_in[i].patient_additional);
        fprintf(stdout, "admincode: %s\n", prop->hdr_in[i].admincode);
        fprintf(stdout, "technician: %s\n", prop->hdr_in[i].technician);
        fprintf(stdout, "equipment: %s\n", prop->hdr_in[i].equipment);
        fprintf(stdout, "recording_additional: %s\n", prop->hdr_in[i].recording_additional);
      }
      snprintf(str2, 1024, "datarecord duration: %lli.%07lli second\n", prop->hdr_in[i].datarecord_duration / EDFLIB_TIME_DIMENSION, prop->hdr_in[i].datarecord_duration % EDFLIB_TIME_DIMENSION);
      remove_trailing_zeros(str2);
      fprintf(stdout, "%s", str2);
      fprintf(stdout, "number of datarecords: %lli\n", prop->hdr_in[i].datarecords_in_file);
      fprintf(stdout, "header length: %i bytes\n", (prop->hdr_in[i].edfsignals + prop->annot_ch_cnt[i] + 1) * 256);
      fprintf(stdout, "number of annotations: %lli\n", prop->hdr_in[i].annotations_in_file);
      fprintf(stdout, "number of annotation channels: %i\n", prop->annot_ch_cnt[i]);
    }

    if(show_signals)
    {
      fprintf(stdout, "+-----+------------------+--------------+------------+----------+----------+----------+----------+-----------+------------+------------+\n"
                      "|  #  |     label        |  samplerate  | smpls per  | digital  | digital  | physical | physical | physical  | resolution | smpls in   |\n"
                      "|     |                  |              | datarecord | maximum  | minimum  | maximum  | minimum  | dimension |            | file       |\n"
                      "+-----+------------------+--------------+------------+----------+----------+----------+----------+-----------+------------+------------+\n");

      for(channel=0; channel<prop->hdr_in[i].edfsignals; channel++)
      {
        p = 0;
        p += snprintf(str1 + p, 4096 - p, "|%4i ", channel + 1);
        p += snprintf(str1 + p, 4096 - p, "| %.16s ", prop->hdr_in[i].signalparam[channel].label);
        snprintf(str2, 128, "%14.10f", ((double)prop->hdr_in[i].signalparam[channel].smp_in_datarecord / (double)prop->hdr_in[i].datarecord_duration) * EDFLIB_TIME_DIMENSION);
        convert_trailing_zeros_to_spaces(str2);
        p += snprintf(str1 + p, 4096 - p, "|%s", str2);
        p += snprintf(str1 + p, 4096 - p, "|%11i ", prop->hdr_in[i].signalparam[channel].smp_in_datarecord);
        p += snprintf(str1 + p, 4096 - p, "|%9i ", prop->hdr_in[i].signalparam[channel].dig_max);
        p += snprintf(str1 + p, 4096 - p, "|%9i ", prop->hdr_in[i].signalparam[channel].dig_min);
        snprintf(str2, 128, "%.9f", prop->hdr_in[i].signalparam[channel].phys_max);
        str2[9] = 0;
        convert_trailing_zeros_to_spaces(str2);
        p += snprintf(str1 + p, 4096 - p, "| %s", str2);
        snprintf(str2, 128, "%.9f", prop->hdr_in[i].signalparam[channel].phys_min);
        str2[9] = 0;
        convert_trailing_zeros_to_spaces(str2);
        p += snprintf(str1 + p, 4096 - p, "| %s", str2);
        p += snprintf(str1 + p, 4096 - p, "| %s  ", prop->hdr_in[i].signalparam[channel].physdimension);
        snprintf(str2, 128, "%.10f", (prop->hdr_in[i].signalparam[channel].phys_max - prop->hdr_in[i].signalparam[channel].phys_min) /
                                     (prop->hdr_in[i].signalparam[channel].dig_max - prop->hdr_in[i].signalparam[channel].dig_min));
        str2[10] = 0;
        convert_trailing_zeros_to_spaces(str2);
        p += snprintf(str1 + p, 4096 - p, "| %s ", str2);
        p += snprintf(str1 + p, 4096 - p, "|%11lli ", prop->hdr_in[i].signalparam[channel].smp_in_file);
        // fprintf(stdout, "prefilter: %s\n", prop->hdr_in[i].signalparam[channel].prefilter);
        // fprintf(stdout, "transducer: %s\n", prop->hdr_in[i].signalparam[channel].transducer);
        fprintf(stdout, "%s|\n", str1);
      }

      fprintf(stdout, "+-----+------------------+--------------+------------+----------+----------+----------+----------+-----------+------------+------------+\n");
    }

    if(show_annot_chns)
    {
      for(j=0; j<prop->annot_ch_cnt[i]; j++)
      {
        fprintf(stdout, "Annotation channel idx: %i   smpls per record: %i\n", prop->annot_ch_idx[i][j] + 1, prop->annot_ch_spr[i][j]);
      }
    }

    if(show_annotations)
    {
      fprintf(stdout, " idx\tonset\tduration\tdescription\n");

      for(int idx=0; idx<prop->hdr_in[i].annotations_in_file; idx++)
      {
        if(sig_flag)
        {
          fprintf(stderr, "\naborted\n");
          return -2;
        }

        if(edf_get_annotation(prop->hdl_in[i], idx, &annot))
        {
          fprintf(stderr, "error: edf_get_annotation()\n");
          return -3;
        }

        if(annot.duration_l >= 0)
        {
          fprintf(stdout, "%i\t%i:%02i:%02i.%07i\t%i.%07i\t%s\n",
                  idx + 1,
                  (int)((annot.onset / EDFLIB_TIME_DIMENSION) / 3600),
                  abs((int)(((annot.onset / EDFLIB_TIME_DIMENSION) / 60) % 60)),
                  abs((int)((annot.onset / EDFLIB_TIME_DIMENSION) % 60)),
                  abs((int)(annot.onset % EDFLIB_TIME_DIMENSION)),
                  abs((int)(annot.duration_l / EDFLIB_TIME_DIMENSION)),
                  abs((int)(annot.duration_l % EDFLIB_TIME_DIMENSION)),
                  annot.annotation);
        }
        else
        {
          fprintf(stdout, "%i\t%i:%02i:%02i.%07i\tn.a.\t%s\n",
                  idx + 1,
                  (int)((annot.onset / EDFLIB_TIME_DIMENSION) / 3600),
                  abs((int)(((annot.onset / EDFLIB_TIME_DIMENSION) / 60) % 60)),
                  abs((int)((annot.onset / EDFLIB_TIME_DIMENSION) % 60)),
                  abs((int)(annot.onset % EDFLIB_TIME_DIMENSION)),
                  annot.annotation);
        }
      }
    }
  }

  return 0;
}













