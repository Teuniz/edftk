/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "utils.h"
#include "edflib.h"
#include "utc_date_time.h"
#include "open_files.h"


extern volatile sig_atomic_t sig_flag;


int open_input_files(prg_args_t *args, f_prop_t *prop)
{
  int i, j, len;

  char *cp=NULL;

  date_time_t dt;

  for(i=0; i<args->in_f_cnt; i++)
  {
    for(j=i-1; j>=0; j--)
    {
      if(!strcmp(args->in_fname[i], args->in_fname[j]))
      {
        fprintf(stderr, "error: duplicate input files\n");
        return -1;
      }
    }
  }

  for(i=0; i<args->in_f_cnt; i++)
  {
    cp = strchr(args->in_fname[i], '=');
    if(cp)
    {
      len = cp - args->in_fname[i] + 1;

      strlcpy(prop->alias_fname[i], args->in_fname[i], len);

      strlcpy(prop->in_fname[i], &args->in_fname[i][len], MAX_PATH_LEN);
    }
    else
    {
      strlcpy(prop->in_fname[i], args->in_fname[i], MAX_PATH_LEN);
    }

    if(edfopen_file_readonly(prop->in_fname[i], &prop->hdr_in[i], EDFLIB_READ_ALL_ANNOTATIONS))
    {
      fprintf(stderr, "%s: ", prop->in_fname[i]);

      switch(prop->hdr_in[i].filetype)
      {
        case EDFLIB_MALLOC_ERROR                : fprintf(stderr, "EDFlib: malloc error\n");
                                                  break;
        case EDFLIB_NO_SUCH_FILE_OR_DIRECTORY   : fprintf(stderr, "EDFlib: can not open file, no such file or directory\n");
                                                  break;
        case EDFLIB_FILE_CONTAINS_FORMAT_ERRORS : fprintf(stderr, "EDFlib: the file is not EDF(+) or BDF(+) compliant\n");
                                                  break;
        case EDFLIB_MAXFILES_REACHED            : fprintf(stderr, "EDFlib: too many files opened\n");
                                                  break;
        case EDFLIB_FILE_READ_ERROR             : fprintf(stderr, "EDFlib: a read error occurred\n");
                                                  break;
        case EDFLIB_FILE_ALREADY_OPENED         : fprintf(stderr, "EDFlib: file has already been opened\n");
                                                  break;
        default                                 : fprintf(stderr, "EDFlib: unknown error\n");
                                                  break;
      }

      for(i=0; i<prop->in_f_cnt; i++)
      {
        edfclose_file(prop->hdl_in[i]);
        prop->hdl_in[i] = -1;
      }

      return -2;
    }

    dt.year = prop->hdr_in[i].startdate_year;
    dt.month = prop->hdr_in[i].startdate_month;
    dt.day = prop->hdr_in[i].startdate_day;
    dt.hour = prop->hdr_in[i].starttime_hour;
    dt.minute = prop->hdr_in[i].starttime_minute;
    dt.second = prop->hdr_in[i].starttime_second;

    date_time_to_utc(&prop->starttime_utc[i], dt);
    prop->starttime_utc[i] *= EDFLIB_TIME_DIMENSION;
    prop->starttime_utc[i] += prop->hdr_in[i].starttime_subsecond;
    prop->endtime_utc[i] = prop->starttime_utc[i] + prop->hdr_in[i].file_duration;

    prop->hdl_in[i] = prop->hdr_in[i].handle;

    prop->in_f_cnt++;

    if(sig_flag)
    {
      fprintf(stderr, "\naborted\n");

      for(i=0; i<prop->in_f_cnt; i++)
      {
        edfclose_file(prop->hdl_in[i]);
        prop->hdl_in[i] = -1;
      }

      return-3;
    }
  }

  for(i=0; i<prop->in_f_cnt; i++)
  {
    if((prop->hdr_in[i].filetype == EDFLIB_FILETYPE_EDFPLUS) || (prop->hdr_in[i].filetype == EDFLIB_FILETYPE_BDFPLUS))
    {
      prop->annot_ch_cnt[i] = edf_get_annot_chan_info(prop->hdl_in[i], prop->annot_ch_idx[i], prop->annot_ch_spr[i]);
      if(prop->annot_ch_cnt[i] < 1)
      {
        fprintf(stderr, "internal error: edf_get_annot_chan_info()\n");
        return -92;
      }
    }
  }

  return 0;
}













