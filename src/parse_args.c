/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2024 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "parse_args.h"


int parse_prog_args(int argc, const char **argv, prg_args_t *args)
{
  int i, len, has_output=0;

  char *cp=NULL;

  for(i=1; i<argc; i++)
  {
    len = strlen(argv[i]);

    if(len < 5)  break;

    if((strcmp(argv[i] + (len-4), ".edf")) &&
       (strcmp(argv[i] + (len-4), ".bdf")) &&
       (strcmp(argv[i] + (len-4), ".rec")) &&
       (strcmp(argv[i] + (len-4), ".REC")) &&
       (strcmp(argv[i] + (len-4), ".EDF")) &&
       (strcmp(argv[i] + (len-4), ".BDF")))  break;

    if(args->in_f_cnt == MAX_INPUT_FILES)
    {
      fprintf(stderr, "too many inputfiles\n");
      return -1;
    }

    args->in_fname[args->in_f_cnt++] = argv[i];
  }

  if(!args->in_f_cnt)
  {
    fprintf(stderr, "missing inputfile\n");
    return -2;
  }

  for(int f=0; f<args->in_f_cnt; f++)
  {
    cp = strchr(args->in_fname[f], '=');
    if(cp)
    {
      len = cp - args->in_fname[f];

      if(len < 1)
      {
        fprintf(stderr, "invalid handle: %s\n", args->in_fname[f]);
        return -3;
      }

      if(strlen(args->in_fname[f]) - len < 6)
      {
        fprintf(stderr, "invalid filename: %s\n", args->in_fname[f]);
        return -3;
      }

      for(int j=0; j<len; j++)
      {
        if(!isupper(args->in_fname[f][j]))
        {
          fprintf(stderr, "invalid handle: %s\n", args->in_fname[f]);
          return -3;
        }
      }
    }
  }

  if(i == argc)
  {
    fprintf(stderr, "missing operation\n");
    return -4;
  }

  args->op = argv[i];

  i++;

  if(i == argc)
  {
    return 0;
  }

  for(; i<argc; i++)
  {
    len = strlen(argv[i]);

    if(!strcmp(argv[i], "output"))
    {
      has_output = 1;
      i++;
      break;
    }

    if(args->op_arg_cnt == MAX_OP_ARGS)
    {
      fprintf(stderr, "too many operation arguments\n");
      return -5;
    }

    args->op_arg[args->op_arg_cnt++] = argv[i];
  }

  if(i == argc)
  {
    if(!has_output)
    {
      return 0;
    }

    fprintf(stderr, "missing output file name\n");
    return -6;
  }

  len = strlen(argv[i]);
  if(len >= 5)
  {
    if((!strcmp(argv[i] + (len-4), ".edf")) ||
       (!strcmp(argv[i] + (len-4), ".rec")) ||
       (!strcmp(argv[i] + (len-4), ".bdf")) ||
       (!strcmp(argv[i] + (len-4), ".EDF")) ||
       (!strcmp(argv[i] + (len-4), ".REC")) ||
       (!strcmp(argv[i] + (len-4), ".BDF")))
    {
      args->out_fname = argv[i];
    }
  }

  if(args->out_fname == NULL)
  {
    fprintf(stderr, "error: invalid output file name\n");
    return -7;
  }

  for(int j=0; j<args->in_f_cnt; j++)
  {
    if(!strcmp(args->in_fname[j], args->out_fname))
    {
      fprintf(stderr, "error: output file name is same as input file name\n");
      return -8;
    }
  }

  i++;

  if(i != argc)
  {
    fprintf(stderr, "error: too many arguments for output\n");
    return -9;
  }

  return 0;
}







