#
#
#

SHELL=/bin/bash

#default is release, for another flavor: make BUILD=debug
BUILD := release

ifneq ($(shell uname), Linux)
  $(error invalid OS)
endif

ifeq ($(BUILD), release)
  $(info build release)
else ifeq ($(BUILD), debug)
  $(info build debug)
else
  $(error invalid value for BUILD)
endif

cflags.common := -std=gnu11 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -Wall -Wextra -Wshadow -Wformat-nonliteral -Wformat-security -Wtype-limits -Wfatal-errors
cflags.debug := -g -O0
cflags.release := -O2 -DNDEBUG

CFLAGS := ${cflags.${BUILD}}  ${cflags.common}
CC := gcc
LDLIBS := -lm

objects := obj/main.o \
           obj/cmp_hdrs.o \
           obj/list_hdrs.o \
           obj/open_files.o \
           obj/edflib.o \
           obj/utils.o \
           obj/utc_date_time.o \
           obj/parse_args.o \
           obj/catenate.o \
           obj/merge.o \
           obj/dump.o \
           obj/sort_files.o \
           obj/annotations.o \
           obj/help.o

headers := src/global.h \
           src/cmp_hdrs.h \
           src/list_hdrs.h \
           src/open_files.h \
           src/edflib.h \
           src/utils.h \
           src/utc_date_time.h \
           src/parse_args.h \
           src/catenate.h \
           src/merge.h \
           src/dump.h \
           src/sort_files.h \
           src/annotations.h \
           src/help.h

all: edftk

edftk : $(objects)
	$(CC) $(objects) -o edftk $(LDLIBS)

obj/main.o : src/main.c $(headers)
	$(CC) $(CFLAGS) -c src/main.c -o obj/main.o

obj/catenate.o : src/catenate.c $(headers)
	$(CC) $(CFLAGS) -c src/catenate.c -o obj/catenate.o

obj/merge.o : src/merge.c $(headers)
	$(CC) $(CFLAGS) -c src/merge.c -o obj/merge.o

obj/dump.o : src/dump.c $(headers)
	$(CC) $(CFLAGS) -c src/dump.c -o obj/dump.o

obj/cmp_hdrs.o : src/cmp_hdrs.c $(headers)
	$(CC) $(CFLAGS) -c src/cmp_hdrs.c -o obj/cmp_hdrs.o

obj/list_hdrs.o : src/list_hdrs.c $(headers)
	$(CC) $(CFLAGS) -c src/list_hdrs.c -o obj/list_hdrs.o

obj/open_files.o : src/open_files.c $(headers)
	$(CC) $(CFLAGS) -c src/open_files.c -o obj/open_files.o

obj/annotations.o : src/annotations.c $(headers)
	$(CC) $(CFLAGS) -c src/annotations.c -o obj/annotations.o

obj/edflib.o : src/edflib.c $(headers)
	$(CC) $(CFLAGS) -c src/edflib.c -o obj/edflib.o

obj/utils.o : src/utils.c $(headers)
	$(CC) $(CFLAGS) -c src/utils.c -o obj/utils.o

obj/utc_date_time.o : src/utc_date_time.c $(headers)
	$(CC) $(CFLAGS) -c src/utc_date_time.c -o obj/utc_date_time.o

obj/parse_args.o : src/parse_args.c $(headers)
	$(CC) $(CFLAGS) -c src/parse_args.c -o obj/parse_args.o

obj/sort_files.o : src/sort_files.c $(headers)
	$(CC) $(CFLAGS) -c src/sort_files.c -o obj/sort_files.o

obj/help.o : src/help.c $(headers)
	$(CC) $(CFLAGS) -c src/help.c -o obj/help.o

clean :
	$(RM) edftk $(objects) test/*.[eb]df

#
#
#
#





