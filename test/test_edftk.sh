#!/bin/bash
#
#
export LC_ALL=C
#
#
vg_opts="--track-origins=yes --leak-check=full --errors-for-leak-kinds=all --exit-on-first-error=yes --error-exitcode=3 -q"
#
#
if ! command -v edfgenerator &> /dev/null
then
    echo "Error, edfgenerator could not be found, make sure it's location is in the PATH environment variable"
    exit 1
fi
#
#
make clean &> /dev/null
#
cd test
#
if [ ${PIPESTATUS[0]} -ne 0 ]
then
  echo "Error, cannot cd into directory test"
  exit 1
fi
#
#
rm *.[eb]df &> /dev/null
#
#
check_error()
{
var1=${PIPESTATUS[0]}
#
if [ ${var1} -eq 3 ]
then
  printf "\nError, valgrind returned an error!\n\n"
  exit 1
fi
#
if [ ${var1} -ne 0 ]
then
  printf "\nError, edftk returned an error!\n\n"
  exit 1
fi
}
#
#
gen8()
{
edfgenerator \
--signals=8 \
--physmax=3000,3000,3000,3000,3000,3000,3000,3000 \
--physmin=-3000,-3000,-3000,-3000,-3000,-3000,-3000,-3000 \
--amp=100,100,100,100,100,100,100,100 \
--wave=sine,sine,sine,sine,sine,sine,ramp,ramp \
--dcycle=50,50,50,50,50,50,100,100 \
--freq=0.625,1.25,1.25,2.5,5,10,0.5,0.05 \
--rate=1579,1999,1197,1755,1333,999,513,1 \
--starttime=$1 \
--len=$2 \
--datrec-duration=$3 \
--type=$4
}
#
#
gen2()
{
edfgenerator \
--signals=2 \
--physmax=3000,3000 \
--physmin=-3000,-3000 \
--amp=100,100 \
--wave=sine,sine \
--dcycle=50,50 \
--freq=0.625,1.7 \
--rate=333,513 \
--starttime=$1 \
--len=$2 \
--datrec-duration=$3 \
--type=$4
}
#
#
gen3()
{
edfgenerator \
--signals=3 \
--physmax=32767,32767,32767 \
--physmin=-32767,-32767,-32767 \
--digmax=32767,32767,32767 \
--digmin=-32767,-32767,-32767 \
--amp=32767,32767,32767 \
--wave=ramp,ramp,ramp \
--dcycle=100,100,100 \
--freq=1,1,1 \
--rate=100 \
--starttime=$1 \
--len=$2 \
--datrec-duration=$3 \
--type=$4
}
#
#
gen4()
{
edfgenerator \
--signals=4 \
--physmax=8388607,8388607,8388607,8388607 \
--physmin=-8388607,-8388607,-8388607,-8388607 \
--digmax=8388607,8388607,8388607,8388607 \
--digmin=-8388607,-8388607,-8388607,-8388607 \
--amp=8388607,8388607,8388607,8388607 \
--wave=ramp,ramp,ramp,ramp \
--dcycle=100,100,100,100 \
--freq=1,1,1,1 \
--rate=100 \
--starttime=$1 \
--len=$2 \
--datrec-duration=$3 \
--type=$4
}
#
#
gen1()
{
edfgenerator \
--signals=2 \
--physmax=32767 \
--physmin=-32767 \
--digmax=32767 \
--digmin=-32767 \
--offset=10000 \
--amp=10000 \
--wave=ramp \
--dcycle=100 \
--freq=1 \
--rate=100 \
--starttime=$1 \
--len=$2 \
--datrec-duration=$3 \
--type=$4
}
#
#
gen8 "00:00:00.333" 30 1 edf
mv edfgenerator.edf 2.edf
#
gen8 "00:00:30.757" 30 1 edf
mv edfgenerator.edf 3.edf
#
gen8 "00:01:00.900" 30 1 edf
mv edfgenerator.edf 1.edf
#
gen8 "00:01:36.000" 30 1 edf
mv edfgenerator.edf 4.edf
#
gen8 "00:02:09.000" 1 1 edf
mv edfgenerator.edf 5.edf
#
#
#
#
gen2 "00:00:00.000" 30 1 edf
mv edfgenerator.edf 10.edf
#
#
gen2 "00:00:03.333" 30 2 edf
mv edfgenerator.edf 11.edf
#
#
gen2 "00:00:04.863" 30 1 edf
mv edfgenerator.edf 12.edf
#
#
#
#
gen2 "00:00:00.000" 10 1 bdf
mv edfgenerator.bdf 20.bdf
#
#
gen2 "00:00:03.333" 12 2 bdf
mv edfgenerator.bdf 21.bdf
#
#
gen2 "00:00:04.863" 15 1 bdf
mv edfgenerator.bdf 22.bdf
#
#
gen2 "00:00:06.350" 1 0.5 bdf
mv edfgenerator.bdf 23.bdf
#
#
#
#
gen8 "00:00:00.000" 30 1 edf
mv edfgenerator.edf 30.edf
#
#
gen8 "00:00:03.333" 30 2 edf
mv edfgenerator.edf 31.edf
#
#
gen8 "00:00:04.863" 30 1 edf
mv edfgenerator.edf 32.edf
#
#
#
#
gen2 "00:00:00.350" 60 0.4 edf
mv edfgenerator.edf 40.edf
#
#
#
gen3 "00:00:00.000" 5 1 edf
mv edfgenerator.edf 50.edf
#
gen4 "00:00:00.000" 5 1 bdf
mv edfgenerator.bdf 51.bdf
#
#
gen1 "00:00:00.000" 5 1 edf
mv edfgenerator.edf 60.edf
#
#
cd ..
#
make BUILD=debug
#
if [ ${PIPESTATUS[0]} -ne 0 ]
then
  echo "Error, make returned an error!"
  exit 1
fi
#
#
#
echo "test 1"
#
valgrind $vg_opts \
./edftk test/1.edf test/2.edf test/3.edf test/4.edf test/5.edf cat maxtimegap=1:10:01 output test/out1.edf
#
check_error
#
#
#
echo "test 2"
#
valgrind $vg_opts \
./edftk test/10.edf test/11.edf test/12.edf merge output test/out2.edf
#
check_error
#
#
#
echo "test 3"
#
valgrind $vg_opts \
./edftk A=test/20.bdf BB=test/21.bdf CCC=test/22.bdf DDDD=test/23.bdf merge output test/out3.bdf
#
check_error
#
#
#
echo "test 4"
#
valgrind $vg_opts \
./edftk B=test/31.edf C=test/32.edf A=test/30.edf merge A3-4 B1-2 C5-end output test/out4.edf
#
check_error
#
#
#
echo "test 5"
#
valgrind $vg_opts \
./edftk test/21.bdf merge output test/out5.bdf
#
check_error
#
#
#
echo "test 6"
#
valgrind $vg_opts \
./edftk A=test/21.bdf merge A2 duration=1:00:00 output test/out6.bdf
#
check_error
#
#
#
echo "test 7"
#
valgrind $vg_opts \
./edftk test/21.bdf cat output test/out7.bdf
#
check_error
#
#
#
echo "test 8"
#
valgrind $vg_opts \
./edftk A=test/21.bdf cat 2 maxtimegap=2:00:03 output test/out8.bdf
#
check_error
#
#
#
echo "test 9"
#
valgrind $vg_opts \
./edftk test/40.edf merge start=0:00:50 duration=0:00:20 output test/out9.edf
#
check_error
#
#
#
echo "test 10"
#
valgrind $vg_opts \
./edftk test/12.edf test/21.bdf merge output test/out10.bdf
#
check_error
#
#
#
echo "test 11"
#
valgrind $vg_opts \
./edftk test/12.edf dump phys 1 1> /dev/null
#
check_error
#
#
#
echo "test 12"
#
valgrind $vg_opts \
./edftk test/12.edf dump hex 2 2 1> /dev/null
#
check_error
#
#
#
echo "test 13"
#
valgrind $vg_opts \
./edftk test/12.edf dump raw 3 3 3 1> /dev/null
#
check_error
#
#
#
echo "test 14"
#
refstring="00000000  302b 1414 2b00 1430  6552 6f63 6472 6e69  |+0...+0.Recordin|
00000008  2067 7473 7261 7374  0014 0000 0000 0000  |g starts........|
00000010  0000 0000 0000 0000  0000 0000 0000 0000  |................|
00000018  0000 0000 0000 0000  0000 0000 0000 0000  |................|
00000020  0000 0000 0000 0000  0000 0000 0000 0000  |................|
00000028  0000 0000 0000 0000  0000 0000 0000 0000  |................|
00000030  0000 0000 0000 0000  0000 0000 0000 0000  |................|
00000038  0000 0000 0000 0000                       |........|"
#
output="$(./edftk test/50.edf dump hex 1 4)"
#
if [ "$refstring" != "$output" ]; then
  printf "\nerror!\n\n"
#
  printf "output:\n"
  printf "$output"
  printf "\n"
#
  printf "expected:\n"
  printf "$refstring"
  printf "\n"
#
  exit 1
fi
#
#
#
echo "test 15"
#
refstring="0000000000  +12331 +05140 +11008 +05168  +25938 +28515 +25714 +28265  |+0...+0.Recordin|
0000000008  +08295 +29811 +29281 +29556  +00020 +00000 +00000 +00000  |g starts........|
0000000016  +00000 +00000 +00000 +00000  +00000 +00000 +00000 +00000  |................|
0000000024  +00000 +00000 +00000 +00000  +00000 +00000 +00000 +00000  |................|
0000000032  +00000 +00000 +00000 +00000  +00000 +00000 +00000 +00000  |................|
0000000040  +00000 +00000 +00000 +00000  +00000 +00000 +00000 +00000  |................|
0000000048  +00000 +00000 +00000 +00000  +00000 +00000 +00000 +00000  |................|
0000000056  +00000 +00000 +00000 +00000                               |........|"
#
output="$(./edftk test/50.edf dump dec 1 4)"
#
if [ "$refstring" != "$output" ]; then
  printf "\nerror!\n\n"
#
  printf "output:\n"
  printf "$output"
  printf "\n"
#
  printf "expected:\n"
  printf "$refstring"
  printf "\n"
#
  exit 1
fi
#
#
#
echo "test 16"
#
refstring="00000000  800001 828f5e 851eba 87ae16  8a3d72 8cccce 8f5c2a 91eb86  |...^........r=....*\....|
00000008  947ae3 970a3f 99999b 9c28f7  9eb853 a147af a3d70b a66668  |.z.?......(.S...G....hf.|
00000010  a8f5c4 ab8520 ae147c b0a3d8  b33334 b5c290 b851ed bae149  |... ..|.....43.....Q.I..|
00000018  bd70a5 c00001 c28f5d c51eb9  c7ae15 ca3d72 ccccce cf5c2a  |.p....]........r=....*\.|"
#
output="$(./edftk test/51.bdf dump hex 1 1 | head -n 4)"
#
if [ "$refstring" != "$output" ]; then
  printf "\nerror!\n\n"
#
  printf "output:\n"
  printf "$output"
  printf "\n"
#
  printf "expected:\n"
  printf "$refstring"
  printf "\n"
#
  exit 1
fi
#
#
#
echo "test 17"
#
refstring="0000000000  -8388606 -8220834 -8053062 -7885290  -7717518 -7549746 -7381974 -7214202  |...^........r=....*\....|"
#
output="$(./edftk test/51.bdf dump dec 2 2 | head -n 1)"
#
if [ "$refstring" != "$output" ]; then
  printf "\nerror!\n\n"
#
  printf "output:\n"
  printf "$output"
  printf "\n"
#
  printf "expected:\n"
  printf "$refstring"
  printf "\n"
#
  exit 1
fi
#
#
#
echo "test 18"
#
refstring="0000000000  -8.389e+06 -8.221e+06 -8.053e+06 -7.885e+06  -7.718e+06 -7.550e+06 -7.382e+06 -7.214e+06  |...^........r=....*\....|"
#
output="$(./edftk test/51.bdf dump phys 2 2 | head -n 1)"
#
if [ "$refstring" != "$output" ]; then
  printf "\nerror!\n\n"
#
  printf "output:\n"
  printf "$output"
  printf "\n"
#
  printf "expected:\n"
  printf "$refstring"
  printf "\n"
#
  exit 1
fi
#
#
echo "test 19"
#
refstring="0000000000  +0.000e+00 +2.000e+02 +4.000e+02 +6.000e+02  +8.000e+02 +1.000e+03 +1.200e+03 +1.400e+03  |......X. .....x.|"
#
output="$(./edftk test/60.edf dump phys 2 2 | head -n 1)"
#
if [ "$refstring" != "$output" ]; then
  printf "\nerror!\n\n"
#
  printf "output:\n"
  printf "$output"
  printf "\n"
#
  printf "expected:\n"
  printf "$refstring"
  printf "\n"
#
  exit 1
fi
#
#
#
printf "all ok\n"
#
exit 0
#

